/****************************************************
*****************************************************
*	HelmetSPILib.c
*
*/

//#ifndef HELMETC
//#define HELMETC
#include <avr/io.h>
#include "HelmetSPILib.h"


void startSPI(void){
	sPIDevice = 0;
	DDRA |= (1<<ACCELSELECTPIN)|(1<<GYROSELECTPIN)|(1<<MEMSELECTPIN)|(1<<4)|(1<<5); //(1<<RADIOSELECTPIN)|
	DDRA &= ~(1<<2);
	PORTA |= (1<<MEMSELECTPIN)|(1<<ACCELSELECTPIN)|(1<<GYROSELECTPIN);
	//SPSR |= (1<<SPI2X);						//Double speed SPI
	SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPR0)|(1<<SPR1);	//Clock Divide is 8 - SPI speed of 1 MHz
}

void deviceSelect(int device){
	switch(device){
		case RADIO:
		SPCR &= ~((1<<CPOL)|(1<<CPHA));
		PORTA &= ~(1<<RADIOSELECTPIN);
		sPIDevice = RADIO;
		break;
		case GYRO:
		SPCR |= ((1<<CPOL)|(1<<CPHA));
		PORTA &= ~(1<<GYROSELECTPIN);
		sPIDevice = GYRO;
		break;
		case ACCEL:
		SPCR |= ((1<<CPOL)|(1<<CPHA));
		PORTA &= ~(1<<ACCELSELECTPIN);
		sPIDevice = ACCEL;
		break;
		case MEM:
		SPCR |= ((1<<CPOL)|(1<<CPHA));
		PORTA &= ~(1<<MEMSELECTPIN);
		sPIDevice = MEM;
		break;
	}
}

char sPITradeByte(char byte){
	SPDR = byte;
	while(!(SPSR & (1<<SPIF) )){}
	char received = SPDR;
	return received;
}

void deselect(void){
	switch(sPIDevice){
		case RADIO:
		PORTA |= (1<<RADIOSELECTPIN);
		break;
		case GYRO:
		PORTA |= (1<<GYROSELECTPIN);
		break;
		case ACCEL:
		PORTA |= (1<<ACCELSELECTPIN);
		break;
		case MEM:
		PORTA |= (1<<MEMSELECTPIN);
		break;
	}
}
//#endif