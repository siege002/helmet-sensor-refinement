/* 	L3G4200D Gyroscope Header File
/	For use with ATtiny44a SPI communication
/	24 June, 2014
/	Sam Siegel
*/



// Register Definitions
#define GYRO_WHO_AM_I 0x0F
#define GYRO_CTRL_REG1 0x20
#define GYRO_CTRL_REG2 0x21
#define GYRO_CTRL_REG3 0x22
#define GYRO_CTRL_REG4 0x23
#define GYRO_CTRL_REG5 0x24
#define GYRO_REFERENCE 0x25
#define GYRO_OUT_TEMP 0x26
#define GYRO_STATUS_REG 0x27
#define GYRO_OUT_X_L 0x28
#define GYRO_OUT_X_H 0x29
#define GYRO_OUT_Y_L 0x2A
#define GYRO_OUT_Y_H 0x2B
#define GYRO_OUT_Z_L 0x2C
#define GYRO_OUT_Z_H 0x2D
#define GYRO_FIFO_CTRL_REG 0X2E
#define GYRO_FIFO_SRC_REG 0X2F
#define GYRO_INT1_CFG 0X30
#define GYRO_INT1_SRC 0X31
#define GYRO_INT1_TSH_XH 0X32
#define GYRO_INT1_TSH_XL 0X33
#define GYRO_INT1_TSH_YH 0X34
#define GYRO_INT1_TSH_YL 0X35
#define GYRO_INT1_TSH_ZH 0X36
#define GYRO_INT1_TSH_ZL 0X37
#define GYRO_INT1_DURATION 0X38

//REGISTER BIT DEFINITIONS
	//CTRL_REG1
#define GYRO_DR1 7
#define GYRO_DR0 6
#define GYRO_BW1 5
#define GYRO_BW0 4
#define GYRO_PD 3
#define GYRO_Zen 2
#define GYRO_Yen 1
#define GYRO_Xen 0
	//CTRL_REG2
#define GYRO_HPM1 5
#define GYRO_HPM0 4
#define GYRO_HPCF3 3
#define GYRO_HPCF2 2
#define GYRO_HPCF1 1
#define GYRO_HPCF0 0
	//CTRL_REG3
#define GYRO_I1_Int1 7
#define GYRO_I1_Boot 6
#define GYRO_H_Lactive 5
#define GYRO_PP_OD 4
#define GYRO_I2_DRDY 3
#define GYRO_I2_WTM 2
#define GYRO_I2_ORun 1
#define GYRO_I2_Empty 0
	//CTRL_REG4
#define GYRO_BDU 7
#define GYRO_BLE 6
#define GYRO_FS1 5
#define GYRO_FS0 4
#define GYRO_ST1 2
#define GYRO_ST0 1
#define GYRO_SIM 0
	//CTRL_REG5
#define GYRO_BOOT 7
#define GYRO_FIFO_EN 6
#define GYRO_HPen 4
#define GYRO_INT1_Sel1 3
#define GYRO_INT1_Sel0 2
#define GYRO_Out_Sel1 1
#define GYRO_Out_Sel0 0
	//REFERENCE
#define GYRO_REF7 7
#define GYRO_REF6 6
#define GYRO_REF5 5
#define GYRO_REF4 4
#define GYRO_REF3 3
#define GYRO_REF2 2
#define GYRO_REF1 1
#define GYRO_REF0 0
	//OUT_TEMP
#define GYRO_TEMP7 7
#define GYRO_TEMP6 6
#define GYRO_TEMP5 5
#define GYRO_TEMP4 4
#define GYRO_TEMP3 3
#define GYRO_TEMP2 2
#define GYRO_TEMP1 1
#define GYRO_TEMP0 0
	//STATUS_REG
#define GYRO_ZYXOR 7
#define GYRO_ZOR 6
#define GYRO_YOR 5
#define GYRO_XOR 4
#define GYRO_ZYXDA 3
#define GYRO_ZDA 2
#define GYRO_YDA 1
#define GYRO_XDA 0
	//FIFO_CTRL_REG
#define GYRO_FM2 7
#define GYRO_FM1 6
#define GYRO_FM0 5
#define GYRO_WTM4 4
#define GYRO_WTM3 3
#define GYRO_WTM2 2
#define GYRO_WTM1 1
#define GYRO_WTM0 0
	//FIFO_SRC_REG
#define GYRO_WTM 7
#define GYRO_OVRN 6
#define GYRO_EMPTY 5
#define GYRO_FSS4 4
#define GYRO_FSS3 3
#define GYRO_FSS2 2
#define GYRO_FSS1 1
#define GYRO_FSS0 0
	//INT1_CFG
#define GYRO_AND_OR 7
#define GYRO_LIR 6
#define GYRO_ZHIE 5
#define GYRO_ZLIE 4
#define GYRO_YHIE 3
#define GYRO_YLIE 2
#define GYRO_XHIE 1
#define GYRO_XLIE 0
	//INT1_SRC
#define GYRO_IA 6
#define GYRO_ZH 5
#define GYRO_ZL 4
#define GYRO_YH 3
#define GYRO_YL 2
#define GYRO_XH 1
#define GYRO_XL 0
	//INT1_THS_XH
#define GYRO_THSX14 6
#define GYRO_THSX13 5
#define GYRO_THSX12 4
#define GYRO_THSX11 3
#define GYRO_THSX10 2
#define GYRO_THSX9 1
#define GYRO_THSX8 0
	//INT1_THS_XL
#define GYRO_THSX7 7
#define GYRO_THSX6 6
#define GYRO_THSX5 5
#define GYRO_THSX4 4
#define GYRO_THSX3 3
#define GYRO_THSX2 2
#define GYRO_THSX1 1
#define GYRO_THSX0 0
	//INT1_THS_YH
#define GYRO_THSY14 6
#define GYRO_THSY13 5
#define GYRO_THSY12 4
#define GYRO_THSY11 3
#define GYRO_THSY10 2
#define GYRO_THSY9 1
#define GYRO_THSY8 0
	//INT1_THS_YL
#define GYRO_THSY7 7
#define GYRO_THSY6 6
#define GYRO_THSY5 5
#define GYRO_THSY4 4
#define GYRO_THSY3 3
#define GYRO_THSY2 2
#define GYRO_THSY1 1
#define GYRO_THSY0 0	
//INT1_THS_ZH
#define GYRO_THSZ14 6
#define GYRO_THSZ13 5
#define GYRO_THSZ12 4
#define GYRO_THSZ11 3
#define GYRO_THSZ10 2
#define GYRO_THSZ9 1
#define GYRO_THSZ8 0
	//INT1_THS_ZL
#define GYRO_THSZ7 7
#define GYRO_THSZ6 6
#define GYRO_THSZ5 5
#define GYRO_THSZ4 4
#define GYRO_THSZ3 3
#define GYRO_THSZ2 2
#define GYRO_THSZ1 1
#define GYRO_THSZ0 0
	//INT1_DURATION
#define GYRO_WAIT 7
#define GYRO_D6 6
#define GYRO_D5 5
#define GYRO_D4 4
#define GYRO_D3 3
#define GYRO_D2 2
#define GYRO_D1 1
#define GYRO_D0 0

//Function Prototypes
void gyroGetData(char output[], int index);
char gyroGetTemp(void);
void gyroWriteReg(char address, char data);
void gyroInitialize(void);
char gyroReadReg(char address);
void gyroSleep(void);
void gyroWake(void);
void gyroPowerDown(void);
char gyroDataReady(void);