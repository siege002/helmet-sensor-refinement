/*
 * GyroTest.c
 *
 * Created: 8/20/2014 12:18:04 PM
 *  Author: Sam
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
//#include "HelmetSPILib.c"
#include "L3G4200D_TINY87.c"
#include "UARTLib.c"

void delay(unsigned long delayTime);
void initSysTime(void);

unsigned long mark;
unsigned long millis;
char gyroRawData[6] = {0};
int i;
long timeStamp;
char neg = 0;
int count = 0;


int main(void)
{
	startSPI();
	initSysTime();
	delay(100);
	uARTStart();
	delay(100);
	uartString("UART READY...\n\r",sizeof("UART READY...\n\r"));
	gyroInitialize();
	delay(1000);
	uartString("GYRO INITIALIZED...\n\r",sizeof("GYRO INITIALIZED...\n\r"));
	char deviceTest =  gyroReadReg(GYRO_WHO_AM_I);
	char devID[8] = "ATAD ON";
	for(int i=0; deviceTest!=0; i++){
		if((deviceTest%2)!=0){
			devID[i] = '1';
			}else{devID[i] = '0';}
			deviceTest = deviceTest/2;
		}
		uartString("GYROSCOPE DEVICE ID:\n\r",sizeof("GYROSCOPE DEVICE ID:\n\r"));
		uartReverse(devID, sizeof(devID));
		uartString("\n\n\r", sizeof("\n\n\r"));
		
    while(1)
    {
        if(gyroDataReady()!=0){
			gyroReadReg(GYRO_WHO_AM_I);
		
			mark = millis;
			gyroGetData(gyroRawData,0);
			char timeout[8] = {0};
			for(int i=7; mark!=0; i--){
				timeout[i] = (mark%10) + '0';
				mark = mark/10;
			}
			char xOut[7];
			char yOut[7];
			char zOut[7];
			//X data processing
			long xRaw = ((long)gyroRawData[0]<<8)|((long)gyroRawData[1]);
			if((xRaw&0b10000000)!=0){
				xRaw = ~xRaw;
				xRaw +=1;
				xOut[0] = '-';
			}
			else{
				xOut[0] = '+';
			}
			long xPros = xRaw*875;
			for(int i=7;i>5;i--){
				xOut[i] = (xPros%10)+'0';
				xPros = xPros/10; 
			}
			xOut[5] = '.';
			for(int i=4;i>0;i--){
				xOut[i] = (xPros%10)+'0';
				xPros = xPros/10;
			}
			//y data processing
			long yRaw = ((long)gyroRawData[2]<<8)|((long)gyroRawData[3]);
			if((yRaw&0b10000000)!=0){
				yRaw = ~yRaw;
				yRaw +=1;
				yOut[0] = '-';
			}
			else{
				yOut[0] = '+';
			}
			long yPros = yRaw*875;
			for(int i=7;i>5;i--){
				yOut[i] = (yPros%10)+'0';
				yPros = yPros/10;
			}
			yOut[5] = '.';
			for(int i=4;i>0;i--){
				yOut[i] = (yPros%10)+'0';
				yPros = yPros/10;
			}
			//z data processing
			long zRaw = ((long)gyroRawData[4]<<8)|((long)gyroRawData[5]);
			if((zRaw&0b10000000)!=0){
				zRaw = ~zRaw;
				zRaw +=1;
				zOut[0] = '-';
			}
			else{
				zOut[0] = '+';
			}
			long zPros = zRaw*875;
			for(int i=7;i>5;i--){
				zOut[i] = (zPros%10)+'0';
				zPros = zPros/10;
			}
			zOut[5] = '.';
			for(int i=4;i>0;i--){
				zOut[i] = (zPros%10)+'0';
				zPros = zPros/10;
			}
			uartString("TimeStamp:  ",sizeof("TimeStamp:  "));
			uartString(timeout, sizeof(timeout));
			uartString(" ms\n\r",sizeof("\n\r"));
			uartString("Xdata:  ",sizeof("Xdata:  "));
			uartString(xOut,sizeof(xOut));
			uartString(" dps\n\r",sizeof(" dps\n\r"));
			uartString("Ydata:  ",sizeof("Ydata:  "));
			uartString(yOut,sizeof(yOut));
			uartString(" dps\n\r",sizeof(" dps\n\r"));
			uartString("Zdata:  ",sizeof("Zdata:  "));
			uartString(zOut,sizeof(zOut));
			uartString(" dps\n\n\r",sizeof(" dps\n\r"));
		}
    }
}

void delay(unsigned long delayTime){
	mark = millis;
	while((millis - mark) <= delayTime){
		;
	}
}



void initSysTime(void){
	TCCR0A = (1<<WGM01);	//CTC mode
	TCCR0B = (1<<CS02);		//Clock prescaler is 64 - 8 MHz/64 = 125 kHZ = 125 cycles/millisecond
	TCNT0 = 0x00;			//Initialize clock counter
	OCR0A = 0x7C;			//124 steps before counter reset
	ASSR = 0x00;
	TIMSK0 = (1<<OCIE0A);	//Enable Clock Compare Interrupts
	millis = 0;
	sei();
}

ISR(TIMER0_COMPA_vect){
	millis += 1;
}

ISR(BADISR_vect){
	
}