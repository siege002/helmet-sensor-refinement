#ifndef HELMETH
#define HELMETH

#define RADIOSELECTPIN 2
#define GYROSELECTPIN 2
#define ACCELSELECTPIN 1
#define MEMSELECTPIN 0
#define RADIO 0
#define GYRO 1
#define ACCEL 2
#define MEM 3


//int sPIDevice;

void startSPI(void);
void deviceSelect(int device);
char sPITradeByte(char byte);
void deselect(void);

#endif