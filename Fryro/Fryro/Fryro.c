/*
 * GyroTest.c
 *
 * Created: 8/20/2014 12:18:04 PM
 *  Author: Sam
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
//#include "HelmetSPILib.c"
#include "L3G4200D_TINY87.c"
#include "ADXL375.c"
//#include "UARTLib.c"

void delay(unsigned long delayTime);
void initSysTime(void);

unsigned long mark;
unsigned long millis;
//long timeStampsAccel [52] = {0};
//long timeStampsGyro [16] = {0};	








int main(void)
{
	startSPI();
	initSysTime();
	delay(100);
	uARTStart();
	gyroWriteReg(GYRO_CTRL_REG5, (1<<GYRO_BOOT));
	accelInitialize();
	delay(10);
	uartString("UART READY...\n\r",sizeof("UART READY...\n\r"));
	delay(10);
	uartString("ACCEL INITIALIZED...\n\r",sizeof("ACCEL INITIALIZED...\n\r"));
	unsigned char deviceTest =  accelReadReg(ACCEL_DEVID_REG);
	unsigned char devID[8] = "ATAD ON";
	for(int i=0; deviceTest!=0; i++){
		if((deviceTest%2)!=0){
			devID[i] = '1';
			}else{devID[i] = '0';}
			deviceTest = deviceTest/2;
		}
	uartString("ACCEL DEVICE ID:  ",sizeof("ACCEL DEVICE ID:  "));
	uartReverse(devID,sizeof(devID));
	uartString("\n\r", sizeof("\n\r"));
	gyroInitialize();
	uartString("GYRO INITIALIZED...\n\r",sizeof("GYRO INITIALIZED...\n\r"));
	unsigned char gyroTest = gyroReadReg(GYRO_WHO_AM_I);
	unsigned char gyroID[8] = "ADAT ON";
	for(int i=0; i<8; i++){
		gyroID[i] = gyroTest%2 + '0';
		gyroTest /= 2;
	}
	uartString("GYRO DEVICE ID:  ",sizeof("GYRO DEVICE ID:  "));
	uartReverse(gyroID,sizeof(gyroID));
	uartString("\n\n\r", sizeof("\n\n\r"));
	uartString("TIME(ms),   Xacc(g),  Yacc(g),  Zacc(g),  XGyro,      YGyro,      ZGyro\n\n\r",sizeof("TIME(ms),   Xacc(g),  Yacc(g),  Zacc(g),  XGyro,      YGyro,      ZGyro\n\n\r"));	
    while(1)
    {
			unsigned char accelRawData[6] = {0};
			unsigned char gyroRawData[6] = {0};
			mark = millis;
			//gather a single data point from each sensor
			while(!(accelGetDataWithCheck(accelRawData,0))){
				;
			}
			while(!(gyroGetDataStatCheck(gyroRawData,0))){
				;
			}
			
			//Format time data for printing
			unsigned char timeDec[10] = {0};
			for(int j=0; j<10; j++){
				timeDec[j] = (mark % 10) + '0';
				mark = mark/10;
			}
			uartReverse(timeDec,sizeof(timeDec));
			uartString(", ",sizeof(", "));
			//Format accel data for printing
			unsigned char xDec[8] = {0};
			unsigned char yDec[8] = {0};
			unsigned char zDec[8] = {0};
			//2-byte shorts to hold each axis' data point
			short xUnfilt = accelRawData[1];
			xUnfilt = xUnfilt<<8;
			xUnfilt |= accelRawData[0];
			short yUnfilt = accelRawData[3];
			yUnfilt = yUnfilt<<8;
			yUnfilt |= accelRawData[2];
			short zUnfilt = accelRawData[5];
			zUnfilt = zUnfilt<<8;
			zUnfilt |= accelRawData[4];
			//conversion from 2's compliment
			if(xUnfilt&0x8000){
				xUnfilt = ~xUnfilt;
				xUnfilt += 1;
				xDec[7] = '-';
			}
			else{
				xDec[7] = '+';
			}
			if(yUnfilt&0x8000){
				yUnfilt = ~yUnfilt;
				yUnfilt += 1;
				yDec[7] = '-';
			}
			else{
				yDec[7] = '+';
			}
			if(zUnfilt&0x8000){
				zUnfilt = ~zUnfilt;
				zUnfilt += 1;
				zDec[7] = '-';
			}
			else{
				zDec[7] = '+';
			}
			//long variable to hold the scaled value	
			unsigned long xScaled = 0; 
			xScaled|= xUnfilt;
			xScaled *= 49;
			unsigned long yScaled = 0;
			yScaled|= yUnfilt;
			yScaled *= 49;
			unsigned long zScaled = 0;
			zScaled |= zUnfilt;
			zScaled *= 49;
			//Conversion to decimal with proper formatting
			for(int j =0; j<3; j++){
				xDec[j] = (xScaled%10)+'0';
				yDec[j] = (yScaled%10)+'0';
				zDec[j] = (zScaled%10)+'0';
				xScaled = xScaled/10;
				yScaled = yScaled/10;
				zScaled = zScaled/10;
			}
			xDec[3] = '.';
			yDec[3] = '.';
			zDec[3] = '.';
			for(int j = 4; j<7; j++){
				xDec[j] = (xScaled%10)+'0';
				yDec[j] = (yScaled%10)+'0';
				zDec[j] = (zScaled%10)+'0';
				xScaled = xScaled/10;
				yScaled = yScaled/10;
				zScaled = zScaled/10;
			}
			//Print results
			uartReverse(xDec,sizeof(xDec));
			uartString(", ",sizeof(", "));
			uartReverse(yDec,sizeof(yDec));
			uartString(", ",sizeof(", "));
			uartReverse(zDec,sizeof(zDec));
			uartString(", ",sizeof(", "));
			//Gyro data processing
			unsigned char GxDec[10] = {0};
			unsigned char GyDec[10] = {0};
			unsigned char GzDec[10] = {0};
			//2-byte Short to hold each axis' data point
			short xGUnfilt = gyroRawData[1];
			xGUnfilt = xGUnfilt <<8;
			xGUnfilt |= gyroRawData[0];
			short yGUnfilt = gyroRawData[3];
			yGUnfilt = yGUnfilt<<8;
			yGUnfilt |= gyroRawData[2];
			short zGUnfilt = gyroRawData[5];
			zGUnfilt = zGUnfilt<<8;
			zGUnfilt |= gyroRawData[4];
			//Conversion from 2's compliment
			if(xGUnfilt&0x8000){
				xGUnfilt = ((~xGUnfilt) + 1);
				GxDec[9] = '-';
			}
			else{
				GxDec[9] = '+';
			}
			if(yGUnfilt&0x8000){
				yGUnfilt = ((~yGUnfilt) + 1);
				GyDec[9] = '-';
			}
			else{
				GyDec[9] = '+';
			}
			if(zGUnfilt&0x8000){
				zGUnfilt = ((~zGUnfilt) + 1);
				GzDec[9] = '-';
			}
			else{
				GzDec[9] = '+';
			}
			//4-byte long to hold each axis' scaled data point
			unsigned long xGScaled = 0;
			xGScaled |= xGUnfilt;
			xGScaled *= 70;
			unsigned long yGScaled = 0;
			yGScaled|= yGUnfilt;
			yGScaled *= 70;
			unsigned long zGScaled = 0;
			zGScaled|= zGUnfilt;
			zGScaled *= 70;
			//Conversion from binary to decimal with proper formatting
			for(int j =0; j<3; j++){
				GxDec[j] = (xGScaled%10)+'0';
				GyDec[j] = (yGScaled%10)+'0';
				GzDec[j] = (zGScaled%10)+'0';
				xGScaled = xGScaled/10;
				yGScaled = yGScaled/10;
				zGScaled = zGScaled/10;
			}
			GxDec[3] = '.';
			GyDec[3] = '.';
			GzDec[3] = '.';
			for(int j = 4; j<9; j++){
				GxDec[j] = (xGScaled%10)+'0';
				GyDec[j] = (yGScaled%10)+'0';
				GzDec[j] = (zGScaled%10)+'0';
				xGScaled = xGScaled/10;
				yGScaled = yGScaled/10;
				zGScaled = zGScaled/10;
			}
			//Print results
			uartReverse(GxDec,sizeof(GxDec));
			uartString(", ",sizeof(", "));
			uartReverse(GyDec,sizeof(GyDec));
			uartString(", ",sizeof(", "));
			uartReverse(GzDec,sizeof(GzDec));
			uartString("\n\r",sizeof("\n\r"));
			delay(500);
	}  //while(1)
}  //main

void delay(unsigned long delayTime){
	mark = millis;
	while((millis - mark) <= delayTime){
		;
	}
}



void initSysTime(void){
	TCCR0A = (1<<WGM01);//|(1<<COM0A0);	//CTC mode
	TCCR0B = (1<<CS00)|(1<<CS01);		//Clock prescaler is 64 - 8 MHz clock, 125 cycles per mS
	OCR0A = 0x7C;			//7 steps before counter reset
	ASSR = 0x00;
	TCNT0 = 0x00;			//Initialize clock counter
	TIMSK0 = (1<<OCIE0A);	//Enable Clock Compare Interrupts
	millis = 0;
	sei();
}

ISR(TIMER0_COMPA_vect){
	millis += 1;
}

ISR(BADISR_vect){
	;
}