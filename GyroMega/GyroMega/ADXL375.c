#include <avr/io.h>
#include "UARTLib.c"
#include "HelmetSPILib.c"
#include "ADXL375.h"


void accelWriteReg(char address, char data);
char accelReadReg(char address);
void accelMultiRead(char address, int bytes, char output[]);
void accelGetData(char output[], int index);
void accelInitialize(void);
char accelCheckInterrupts(void);
void accelSleep(void);



/****************************************************
* accelWriteReg
* writes a single character to a single register
* PARAMS - address - ADXL375 address to write to
*        - data - data to write
******************************************************/
void accelWriteReg(char address, char data){
	address &= 0x3F;
	deviceSelect(ACCEL);
	sPITradeByte(address);
	sPITradeByte(data);
	deselect();
}

/****************************************************
* acccleReadReg
* reads a single register
* PARAMS - address - address of register to read
* RETURNS - data read from register
*****************************************************/
char accelReadReg(char address){
	address |= 0x80;
	deviceSelect(ACCEL);
	sPITradeByte(address);
	char data = sPITradeByte(0);
	deselect();
	return data;
}
/****************************************************
* accelMultiRead
* reads multiple bytes from accelerometer using auto-increment
* PARAMS - address - starting accelerometer address to read from
*        - bytes - number of bytes to read
*        - output - array to hold output values
******************************************************/
void accelMultiRead(char address, int bytes, char output[]){
	address|=0xC0;
	deviceSelect(ACCEL);
	sPITradeByte(address);
	for(int i=0; i<bytes; i++){
		output[i] = sPITradeByte(0);
	}
	deselect();
}
/***************************************************
* accleGetData
* gets accelerometer data for each axis
* PARAMS: output - array to hold output data
		  index - starting index for writing data - allows multiple reads to one large array
*****************************************************/

void accelGetData(char output[], int index){
	char temp[6] = {0};
	accelMultiRead(ACCEL_DATAX0_REG, 6, temp);
	for(int i=0;i<6;i++){
		output[i+index] = temp[i];
	}
}

/*****************************************************
* accelInitialize
* Initializes the accelerometer with the settings as described below
* TODO - test and calibrate offset values, threshold values
****************************************************/

void accelInitialize(void){
	DDRC &= ~(1<<PINC5);
	char accelParams[][2] = {
		{ ACCEL_INT_ENABLE_REG, 0x00},		//Disable all interrupts before configuration
		{ ACCEL_OFSX_REG, 0xFD},
		{ ACCEL_OFSY_REG, 0x01},
		{ ACCEL_OFSZ_REG, 0x80},	
		{ ACCEL_THRESH_SHOCK_REG, 0x09}, 	// 780 mg * 9 LSB = 7 G for shock indication
		{ ACCEL_OFSX_REG, 0x00},			// Must be calibrated with testing
		{ ACCEL_OFSY_REG, 0x00},			// Must be calibrated with testing
		{ ACCEL_OFSZ_REG, 0x00},			// Must be calibrated with testing
		{ ACCEL_DUR_REG, 0x01},				// minimum duration for shock = 625 uS
		{ ACCEL_LATENT_REG, 0x00},			//Double shock disabled
		{ ACCEL_WINDOW_REG, 0x00},			//Double shock disabled
		{ ACCEL_THRESH_ACT_REG, 0x03},		//activity threshold at 780 mg * 3 LSB = 2.34 g
		{ ACCEL_THRESH_INACT_REG, 0x03},	//inactivity threshold same as activity
		{ ACCEL_TIME_INACT_REG, 0x0A},		//inactivity detected after ten seconds below threshold - 1 sec/LSB
		{ ACCEL_ACT_INACT_CTL_REG, 0x77},	//All axes enabled for activity/inactivity, DC coupled for both
		{ ACCEL_SHOCK_AXES_REG, 0x0F},		//All axes participate in shock detection
		{ ACCEL_BW_RATE_REG, 0xFF},			//3200 Hz ODR - .0977 g/lsb
		{ ACCEL_POWER_CTL_REG, 0x08 },		//Part is in standby mode, Link and autosleep functions enabled
		{ ACCEL_DATA_FORMAT_REG, 0x0B},		//Self test off, 4-wire SPI mode, interrupts active high, Right justified data
		{ ACCEL_FIFO_CTL, 0xCC},			//Stream mode, trigger bit meaningless, Samples bits meaningless - no watermark int.	
		{ 255,0}
	};
	for(int i=0; accelParams[i][0]!= 255; i++){
		accelWriteReg(accelParams[i][0], accelParams[i][1]);
		//uartString("Accel write OK\n\r",sizeof"Accel write OK\n\r");
	}
	accelWriteReg(ACCEL_INT_ENABLE_REG, (ACCEL_DATA_READY|ACCEL_SINGLE_SHOCK|ACCEL_ACTIVITY));	//Interrupts for single shock, activity, and data ready - read status register to determine which interrupt was enacted.
}

/*****************************************************
* accelCheckInterrupts
* returns the value of the interrupt source register
* TODO - PINA2 and PINA3 need to be changed to match the actual pins chosen connection to the interrupts
******************************************************/
char accelCheckInterrupts(void){
	char interrupts = 0x00;
	if(PINC&(1<<PINC5)){
		interrupts = accelReadReg(ACCEL_INT_SOURCE_REG);
	}
	return interrupts;
}

void accelSleep(void){
	accelWriteReg(ACCEL_POWER_CTL_REG, 0x04);
}