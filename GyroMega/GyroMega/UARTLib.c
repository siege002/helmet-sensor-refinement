#include <avr/io.h>


void uARTStart(void);
void writeUart(char toSend);
void uartString(char toSend[], char bytes);
void uartReverse(char toSend[], char bytes);

void uARTStart(void){
DDRD |= (1<<PIND1); //Set TXD pin as output
UBRR0L = 0x01;		//Buad = Fosc/(2*(UBRR0+1)) - 8MHz/400 = 20000 BAUD
UBRR0H = 0x00;
UCSR0C = (1<<UCSZ01)|(1<<UCSZ00);	//Synchronous operation, 8 data-bits, no parity, 1 stop bit
UCSR0B = (1<<TXEN0);
	
}

void writeUart(char toSend){
	//while(!(UCSR0A&(1<<UDRE0))){}
	UDR0 = toSend;
	while(!(UCSR0A&(1<<TXC0))){}
	UCSR0A |= (1<<TXC0);
}

void uartString(char toSend[], char bytes){
	for(int p=0; p<bytes; p++){
		writeUart(toSend[p]);
	}
}

void uartReverse(char toSend[], char bytes){
	for(int p=(bytes-1);p>=0;p--){
		writeUart(toSend[p]);
	}
}