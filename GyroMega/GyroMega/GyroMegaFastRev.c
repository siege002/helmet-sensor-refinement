/*
 * GyroTest.c
 *
 * Created: 8/20/2014 12:18:04 PM
 *  Author: Sam
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
//#include "HelmetSPILib.c"
#include "L3G4200D_TINY87.c"
#include "ADXL375.c"
//#include "UARTLib.c"

void delay(unsigned long delayTime);
void initSysTime(void);

unsigned long mark;
unsigned long millis;
long timeStampsAccel [52] = {0};
long timeStampsGyro [16] = {0};	
char accelRawData[300] = {0};
char gyroRawData[80] = {0};	








int main(void)
{
	startSPI();
	initSysTime();
	delay(100);
	uARTStart();
	accelInitialize();
	delay(10);
	gyroInitialize();
	uartString("UART READY...\n\r",sizeof("UART READY...\n\r"));
	delay(10);
	uartString("ACCEL INITIALIZED...\n\r",sizeof("ACCEL INITIALIZED...\n\r"));
	char deviceTest =  accelReadReg(ACCEL_DEVID_REG);
	char devID[8] = "ATAD ON";
	for(int i=0; deviceTest!=0; i++){
		if((deviceTest%2)!=0){
			devID[i] = '1';
			}else{devID[i] = '0';}
			deviceTest = deviceTest/2;
		}
	uartString("ACCEL DEVICE ID:\n\r",sizeof("ACCEL DEVICE ID:\n\r"));
	uartReverse(devID,sizeof(devID));
	uartString("\n\n\r", sizeof("\n\n\r"));
	uartString(" TIME(ms),X DATA(g),Y DATA(g),Z DATA(g),XGyro,YGyro,ZGyro\n\n\r",sizeof(" TIME(ms),X DATA(g),Y DATA(g),Z DATA(g),XGyro,YGyro,ZGyro\n\n\r"));	
    while(1)
    {
			int a=0;
			int b=0;
			int tA=0;
			int tG=0;
			if(accelCheckInterrupts()&ACCEL_SINGLE_SHOCK){
			mark = millis;
			for(int i=0;i<5;i++){/*asm("NOP");*/}
			while(millis-mark <= 15){
				if(accelCheckInterrupts()&ACCEL_DATA_READY){
					for(int i=0;i<40;i++){/*asm("NOP");*/}
					accelGetData(accelRawData,a);
					a += 6;
//					timeStampsAccel[tA] = millis;
//					tA ++;
				}
				if(gyroDataReady()){
					gyroGetData(gyroRawData,b);
					b += 6;
//					timeStampsGyro[tG] = millis;
//					tG++;
				}
			}
			char timeDec[10] = {0};
			for(int j=0; mark !=0; j++){
				timeDec[j] = (mark % 10) + '0';
				mark = mark/10;
			}
			uartReverse(timeDec,sizeof(timeDec));
			uartString("\n\r",sizeof("\n\r"));
			for(int i =0; i<(a/6); i++){

				char xDec[9] = {0};
				char yDec[9] = {0};
				char zDec[9] = {0};
				long xUnfilt = (accelRawData[0+(6*i)])|(accelRawData[1+(6*i)]);
				long yUnfilt = (accelRawData[2+(6*i)])|(accelRawData[3+(6*i)]);
				long zUnfilt = (accelRawData[4+(6*i)])|(accelRawData[5+(6*i)]);
				if(xUnfilt&0x8000){
					xUnfilt = ((~xUnfilt) + 1)&0xFF;
					xDec[8] = '-';
				}else{xDec[8] = '+';}
				if(yUnfilt&0x8000){
					yUnfilt = ((~yUnfilt) + 1)&0xFF;
					yDec[8] = '-';
				}else{yDec[8] = '+';}
				if(zUnfilt&0x8000){
					zUnfilt = ((~zUnfilt) + 1)&0xFF;
					zDec[8] = '-';
				}else{zDec[8] = '+';}
				xUnfilt = xUnfilt*977;
				yUnfilt = yUnfilt*977;
				zUnfilt = zUnfilt*977;
				for(int j =0; j<4; j++){
					xDec[j] = (xUnfilt%10)+'0';
					yDec[j] = (yUnfilt%10)+'0';
					zDec[j] = (zUnfilt%10)+'0';
					xUnfilt = xUnfilt/10;
					yUnfilt = yUnfilt/10;
					zUnfilt = zUnfilt/10;
				}
				xDec[4] = '.';
				yDec[4] = '.';
				zDec[4] = '.';
				for(int j = 5; j<8; j++){
					xDec[j] = (xUnfilt%10)+'0';
					yDec[j] = (yUnfilt%10)+'0';
					zDec[j] = (zUnfilt%10)+'0';
					xUnfilt = xUnfilt/10;
					yUnfilt = yUnfilt/10;
					zUnfilt = zUnfilt/10;
				}

				uartReverse(xDec,sizeof(xDec));
				uartString(",",sizeof(","));
				uartReverse(yDec,sizeof(yDec));
				uartString(",",sizeof(","));
				uartReverse(zDec,sizeof(zDec));
				uartString("\n\r",sizeof("\n\r"));
			}
			for(int i=0; i<(b/6); i++){
				char xDec[10] = {0};
				char yDec[10] = {0};
				char zDec[10] = {0};
				long xUnfilt = (gyroRawData[0+(6*i)])|((long)gyroRawData[1+(6*i)]<<8);
				long yUnfilt = (gyroRawData[2+(6*i)])|((long)gyroRawData[3+(6*i)]<<8);
				long zUnfilt = (gyroRawData[4+(6*i)])|((long)gyroRawData[5+(6*i)]<<8);
				if(xUnfilt&0x8000){
					xUnfilt = ((~xUnfilt) + 1)&0xFF;
					xDec[9] = '-';
				}else{xDec[9] = '+';}
				if(yUnfilt&0x8000){
					yUnfilt = ((~yUnfilt) + 1)&0xFF;
					yDec[9] = '-';
				}else{yDec[9] = '+';}
				if(zUnfilt&0x8000){
					zUnfilt = ((~zUnfilt) + 1)&0xFF;
					zDec[9] = '-';
				}else{zDec[9] = '+';}
				xUnfilt = xUnfilt*875;
				yUnfilt = yUnfilt*875;
				zUnfilt = zUnfilt*875;
				for(int j =0; j<5; j++){
					xDec[j] = (xUnfilt%10)+'0';
					yDec[j] = (yUnfilt%10)+'0';
					zDec[j] = (zUnfilt%10)+'0';
					xUnfilt = xUnfilt/10;
					yUnfilt = yUnfilt/10;
					zUnfilt = zUnfilt/10;
				}
				xDec[5] = '.';
				yDec[5] = '.';
				zDec[5] = '.';
				for(int j = 6; j<9; j++){
					xDec[j] = (xUnfilt%10)+'0';
					yDec[j] = (yUnfilt%10)+'0';
					zDec[j] = (zUnfilt%10)+'0';
					xUnfilt = xUnfilt/10;
					yUnfilt = yUnfilt/10;
					zUnfilt = zUnfilt/10;
				}
				uartReverse(xDec,sizeof(xDec));
				uartString(",",sizeof(","));
				uartReverse(yDec,sizeof(yDec));
				uartString(",",sizeof(","));
				uartReverse(zDec,sizeof(zDec));
				uartString("\n\r",sizeof("\n\r"));
			}
			a=0;
			b=0;
			tA=0;
			tG=0;
		}			
	}
}

void delay(unsigned long delayTime){
	mark = millis;
	while((millis - mark) <= delayTime){
		;
	}
}



void initSysTime(void){
	TCCR0A = (1<<WGM01);//|(1<<COM0A0);	//CTC mode
	TCCR0B = (1<<CS00)|(1<<CS01);		//Clock prescaler is 64 - 8 MHz clock, 125 cycles per mS
	OCR0A = 0x7C;			//7 steps before counter reset
	ASSR = 0x00;
	TCNT0 = 0x00;			//Initialize clock counter
	TIMSK0 = (1<<OCIE0A);	//Enable Clock Compare Interrupts
	millis = 0;
	sei();
}

ISR(TIMER0_COMPA_vect){
	millis += 1;
}

ISR(BADISR_vect){
	;
}