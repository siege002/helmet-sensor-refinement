/*
 * GyroTest.c
 *
 * Created: 8/20/2014 12:18:04 PM
 *  Author: Sam
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
//#include "HelmetSPILib.c"
#include "L3G4200D_TINY87.c"
#include "ADXL375.c"
//#include "UARTLib.c"

void delay(unsigned long delayTime);
void initSysTime(void);
void flipLED(void);

unsigned long mark;
unsigned long millis;
//long timeStampsAccel [52] = {0};
//long timeStampsGyro [16] = {0};	








int main(void)
{
	startSPI();
	initSysTime();
	delay(100);
	uARTStart();
	gyroWriteReg(GYRO_CTRL_REG5, (1<<GYRO_BOOT));
	accelInitialize();
	delay(10);
	DDRC|=(1<<4); //LED output port
    while(1)
    {
		unsigned char accelRawData[288] = {0};
		unsigned char gyroRawData[72] = {0};
		if(accelCheckInterrupts()&ACCEL_SINGLE_SHOCK){
			mark = millis;
			int accelIndex = 0;
			int gyroIndex = 0;
			while(accelIndex<288|gyroIndex<72){
				if(accelIndex<288){
					if(accelGetDataWithCheck(accelRawData,accelIndex)){
						accelIndex+=6;
					}
				}
				if((gyroIndex<72)&(accelIndex>92)){
					if(gyroGetDataStatCheck(gyroRawData,gyroIndex)){
						gyroIndex+=6;
					}
				}
			}
			while(1){
				unsigned char timeDec[10] = {0};
				for(int j=0; j<10; j++){
					timeDec[j] = (mark % 10) + '0';
					mark = mark/10;
				}
				uartString("Timestamp (ms): ",sizeof("Timestamp (ms): "));
				uartReverse(timeDec,sizeof(timeDec));
				uartString("\n\r",sizeof("\n\r"));
				uartString("AccelX, AccelY, AccelZ, GyroX, GyroY, GyroZ\n\n\r",sizeof("AccelX, AccelY, AccelZ, GyroX, GyroY, GyroZ\n\n\r"));
				int gyroCounter = 0;
				for(int i=0; i<288; i+=6){
					unsigned char accelXDec[8] = {0};
					unsigned char accelYDec[8] = {0};
					unsigned char accelZDec[8] = {0};
					unsigned char gyroXDec[8] = {0};
					unsigned char gyroYDec[8] = {0};
					unsigned char gyroZDec[8] = {0};
					unsigned short accelXUnfilt = accelRawData[i+1];
					accelXUnfilt = accelXUnfilt<<8;
					accelXUnfilt |= accelRawData[i];
					unsigned short accelYUnfilt = accelRawData[i+3];
					accelYUnfilt = accelYUnfilt<<8;
					accelYUnfilt |= accelRawData[i+2];
					unsigned short accelZUnfilt = accelRawData[i+5];
					accelZUnfilt = accelZUnfilt<<8;
					accelZUnfilt |= accelRawData[i+4];
					if(accelXUnfilt & 0x8000){
						accelXUnfilt = (~accelXUnfilt)+1;
						accelXDec[7] = '-';
					}
					else{accelXDec[7] = '+';}
					if(accelYUnfilt & 0x8000){
						accelYUnfilt = (~accelYUnfilt)+1;
						accelYDec[7] = '-';
					}
					else{accelYDec[7] = '+';}
					if(accelZUnfilt & 0x8000){
						accelZUnfilt = (~accelZUnfilt)+1;
						accelZDec[7] = '-';
					}
					else{accelZDec[7] = '+';}
					unsigned long accelXScaled = ((unsigned long)accelXUnfilt)*49;
					unsigned long accelYScaled = ((unsigned long)accelYUnfilt)*49;
					unsigned long accelZScaled = ((unsigned long)accelZUnfilt)*49;
					for(int j=0; j<3; j++){
						accelXDec[j] = accelXScaled%10 + '0';
						accelXScaled /= 10;
						accelYDec[j] = accelYScaled%10 + '0';
						accelYScaled /= 10;
						accelZDec[j] = accelZScaled%10 + '0';
						accelZScaled /= 10;
					}
					accelXDec[3] = '.';
					accelYDec[3] = '.';
					accelZDec[3] = '.';
					for(int j =4; j<7; j++){
						accelXDec[j] = accelXScaled%10 + '0';
						accelXScaled /= 10;
						accelYDec[j] = accelYScaled%10 + '0';
						accelYScaled /= 10;
						accelZDec[j] = accelZScaled%10 + '0';
						accelZScaled /= 10;
					}
					uartReverse(accelXDec,sizeof(accelXDec));
					uartString(" , ",sizeof(" , "));
					uartReverse(accelYDec,sizeof(accelYDec));
					uartString(" , ",sizeof(" , "));
					uartReverse(accelZDec,sizeof(accelZDec));
					uartString(" , ",sizeof(" , "));
					if(gyroCounter%4 == 0){
						unsigned short gyroXUnfilt =  gyroRawData[(i/4)+1];
						gyroXUnfilt = gyroXUnfilt<<8;
						gyroXUnfilt |= gyroRawData[(i/4)];
						unsigned short gyroYUnfilt =  gyroRawData[(i/4)+3];
						gyroYUnfilt = gyroYUnfilt<<8;
						gyroYUnfilt |= gyroRawData[(i/4)+2];
						unsigned short gyroZUnfilt =  gyroRawData[(i/4)+5];
						gyroZUnfilt = gyroZUnfilt<<8;
						gyroZUnfilt |= gyroRawData[(i/4)+4];
						if(gyroXUnfilt&0x8000){
							gyroXUnfilt = (!gyroXUnfilt)+1;
							gyroXDec[7] = '-';
						}
						else{gyroXDec[7] = '+';}
						if(gyroYUnfilt&0x8000){
							gyroYUnfilt = (!gyroYUnfilt)+1;
							gyroYDec[7] = '-';
						}
						else{gyroYDec[7] = '+';}
						if(gyroZUnfilt&0x8000){
							gyroZUnfilt = (!gyroZUnfilt)+1;
							gyroZDec[7] = '-';
						}
						else{gyroZDec[7] = '+';}
						unsigned long gyroXScaled = ((unsigned long)gyroXUnfilt)*7;
						unsigned long gyroYScaled = ((unsigned long)gyroYUnfilt)*7;
						unsigned long gyroZScaled = ((unsigned long)gyroZUnfilt)*7;
						for(int j=0; j<2; j++){
							gyroXDec[j] = (gyroXScaled%10)+'0';
							gyroXScaled /= 10;
							gyroYDec[j] = (gyroYScaled%10)+'0';
							gyroYScaled /= 10;
							gyroZDec[j] = (gyroZScaled%10)+'0';
							gyroZScaled /= 10;
						}
						gyroXDec[2] = '.';
						gyroYDec[2] = '.';
						gyroZDec[2] = '.';
						for(int j =0; j<7; j++){
							gyroXDec[j] = (gyroXScaled%10)+'0';
							gyroXScaled /= 10;
							gyroYDec[j] = (gyroYScaled%10)+'0';
							gyroYScaled /= 10;
							gyroZDec[j] = (gyroZScaled%10)+'0';
							gyroZScaled /= 10;
						}
						uartReverse(gyroXDec,sizeof(gyroXDec));
						uartString(" , ",sizeof(" , "));
						uartReverse(gyroYDec,sizeof(gyroYDec));
						uartString(" , ",sizeof(" , "));
						uartReverse(gyroZDec,sizeof(gyroZDec));
						uartString("\n\r",sizeof("\n\r"));
						}else{uartString("\n\r",sizeof("\n\r"));}
						gyroIndex++;
					}
					delay(1000);
					flipLED();
				}
			}
	}  //while(1)
}  //main

void delay(unsigned long delayTime){
	mark = millis;
	while((millis - mark) <= delayTime){
		;
	}
}



void initSysTime(void){
	TCCR0A = (1<<WGM01);//|(1<<COM0A0);	//CTC mode
	TCCR0B = (1<<CS00)|(1<<CS01);		//Clock prescaler is 64 - 8 MHz clock, 125 cycles per mS
	OCR0A = 0x7C;			//7 steps before counter reset
	ASSR = 0x00;
	TCNT0 = 0x00;			//Initialize clock counter
	TIMSK0 = (1<<OCIE0A);	//Enable Clock Compare Interrupts
	millis = 0;
	sei();
}

void flipLED(void){
	if(PORTC&(1<<4)){
		PORTC &= ~(1<<4);
	}
	else{
		PORTC |= (1<<4);
	} 
}

ISR(TIMER0_COMPA_vect){
	millis += 1;
}

ISR(BADISR_vect){
	;
}