/* 	MX25L4006E Header File
/	Definitions for Macronix Flash Memory
/	to be used with ATTiny44a
/	24 June, 2014
/	Sam Siegel
*/

//Register/Command Definitions
#define MEM_WREN 0x06	//Write Enable 
#define MEM_WRDI 0x04	//Write Disable
#define MEM_WRSR 0x01	//Write Status Register
#define MEM_RDID 0x9F	//Read Identification
#define MEM_RDSR 0x05	//Read Status Register
#define MEM_READ 0x03	//Read data, provide 3-byte address
#define MEM_FREAD 0x0B	//Fast Read, provide 3-byte address + dummy
#define MEM_RDSFDP 0x5A //Read serial flash discoverable parameters,
						//provide 3-byte address and 4th dummy byte
#define MEM_RES 0xAB	//Read Electronic ID
#define MEM_REMS 0x90	//Read Electronic manufacturer & device ID
#define MEM_DREAD 0x3B	//Double output mode, provide 3-byte add. and 4'th dummy
#define MEM_SE 0x20		//Sector Erase, provide 3-byte address
#define MEM_BE 0xD8		//Block Erase, provide 3-byte address
#define MEM_CE 0xC7		//Chip Erase
#define MEM_PP 0x02		//Page Program, provide 3 byte address
#define MEM_DP 0xB9		//Enter Deep Power Down mode
#define MEM_RDP 0xAB	//Release from deep power down mode

//Bit definitions
	//Read/Write Status Register - only write SRWP, BP2-0
#define MEM_SRWP 7 	//Status Register Write Protect
#define MEM_BP2 4 	//Block protection bits
#define MEM_BP1 3
#define MEM_BP0 2
#define MEM_WEL 1	//Write Enable Latch
#define MEM_WIP 0	//Write in progress bit

//Function Prototypes
void memReadData(long address, int bytes, char output[], int index);
void memWritePage(char data[static 256], long address);
void memWriteBytes(char data[], int size, long address);
void memWaitForWrite(void);