#define ACCEL_DEVID_REG          0x00
#define ACCEL_THRESH_SHOCK_REG   0x1D
#define ACCEL_OFSX_REG           0x1E
#define ACCEL_OFSY_REG           0x1F
#define ACCEL_OFSZ_REG           0x20
#define ACCEL_DUR_REG            0x21
#define ACCEL_LATENT_REG         0x22
#define ACCEL_WINDOW_REG         0x23
#define ACCEL_THRESH_ACT_REG     0x24
#define ACCEL_THRESH_INACT_REG   0x25
#define ACCEL_TIME_INACT_REG     0x26
#define ACCEL_ACT_INACT_CTL_REG  0x27
#define ACCEL_THRESH_FF_REG      0x28
#define ACCEL_TIME_FF_REG        0x29
#define ACCEL_SHOCK_AXES_REG       0x2A
#define ACCEL_ACT_SHOCK_STATUS_REG 0x2B
#define ACCEL_BW_RATE_REG        0x2C
#define ACCEL_POWER_CTL_REG      0x2D
#define ACCEL_INT_ENABLE_REG     0x2E
#define ACCEL_INT_MAP_REG        0x2F
#define ACCEL_INT_SOURCE_REG     0x30
#define ACCEL_DATA_FORMAT_REG    0x31
#define ACCEL_DATAX0_REG         0x32
#define ACCEL_DATAX1_REG         0x33
#define ACCEL_DATAY0_REG         0x34
#define ACCEL_DATAY1_REG         0x35
#define ACCEL_DATAZ0_REG         0x36
#define ACCEL_DATAZ1_REG         0x37
#define ACCEL_FIFO_CTL           0x38
#define ACCEL_FIFO_STATUS        0x39
 
//Data rate codes.
#define ACCEL_3200HZ      0x0F
#define ACCEL_1600HZ      0x0E
#define ACCEL_800HZ       0x0D
#define ACCEL_400HZ       0x0C
#define ACCEL_200HZ       0x0B
#define ACCEL_100HZ       0x0A
#define ACCEL_50HZ        0x09
#define ACCEL_25HZ        0x08
#define ACCEL_12HZ5       0x07
#define ACCEL_6HZ25       0x06
 
#define ACCEL_SPI_READ    0x80
#define ACCEL_SPI_WRITE   0x00
#define ACCEL_MULTI_BYTE  0x40
 
#define ACCEL_X           0x00
#define ACCEL_Y           0x01
#define ACCEL_Z           0x02

#define ACCEL_DATA_READY		0x80
#define ACCEL_SINGLE_SHOCK		0x40
#define ACCEL_DOUBLE_SHOCK		0x20
#define ACCEL_ACTIVITY			0x10
#define ACCEL_INACTIVITY		0x08
#define ACCEL_WATERMARK			0x02
#define ACCEL_OVERRUN			0x01

#define DEVICE (0x53)    //ADXL345 device address


#define TO_READ (6)        //num of bytes we are going to read each time (two bytes for each axis)
