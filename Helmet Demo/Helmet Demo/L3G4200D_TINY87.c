/* 	L3G4200D Library 
/	for use with ATtiny87 SPI communication
/	30 June, 2014
/	Sam Siegel
/	Basic Operation:  Gyro runs in stream mode, generates and interrupt (INT2) when the FIFO is not empty.
/	Main program should periodically check the interrupt pin and read data out until the interrupt pin goes low.
/	
*/


#include <avr/io.h>
#include "L3G4200D_TINY87.h"
#include "HelmetSPILib.c"

#define gyroDataReadyPin	4		//PORTC, PC4
#define gyroRead			0x80
#define gyroWrite			0x00
#define increment	 		0x40
#define noIncrement			0x00


//Fills a supplied array with gyro data, starting at the given index number
void gyroGetData(unsigned char output[], int index){
	deviceSelect(GYRO);
	unsigned char toSend = GYRO_OUT_X_L | increment | gyroRead;
	sPITradeByte(toSend);							//Address of Xlow register, w/autoincrement
	for(int i=0; i<6; i++){
		output[i+index] = sPITradeByte(0x00);
	}
	deselect();
}

// Returns current temperature of gyro	
unsigned char gyroGetTemp(void){
	unsigned char gyroTemp;
	deviceSelect(GYRO);
	unsigned char tempAddress = GYRO_OUT_TEMP | gyroRead;
	sPITradeByte(tempAddress);
	gyroTemp = sPITradeByte(0);
	deselect();
	return gyroTemp;
	}

void gyroWriteReg(unsigned char address, unsigned char data){
	address &= 0x3F;
	deviceSelect(GYRO);
	sPITradeByte(address);
	sPITradeByte(data);
	deselect();
}

void gyroInitialize(void){
	//DDRC &= ~(1<<gyroDataReadyPin);	//Connected to INT2 of Gyro, configured as input
	unsigned char gyroParams [] [2] = {
		{ GYRO_CTRL_REG1, 0xEF },
		{ GYRO_CTRL_REG2, (1<<GYRO_HPM1) },
		//{ GYRO_CTRL_REG3, (1<<GYRO_I2_WTM) },
		{ GYRO_CTRL_REG4, (1<<GYRO_BDU)|(1<<GYRO_FS1)|(1<<GYRO_FS0)},
		{ GYRO_CTRL_REG5, (1<<GYRO_FIFO_EN) },
		{ GYRO_FIFO_CTRL_REG, (1<<GYRO_FM1)}, //Stream Mode, FIFO not empty interrupt threshold
		{ 255,0}
	};

	for(int i=0; gyroParams[i][0] != 255; i++){
		gyroWriteReg(gyroParams[i][0], gyroParams[i][1]);
	}

}

unsigned char gyroReadReg(unsigned char address){
	address |= gyroRead;
	deviceSelect(GYRO);
	sPITradeByte(address);
	unsigned char contents = sPITradeByte(0);
	deselect();
	return contents;
}

void gyroSleep(void){
	unsigned char gyroState = gyroReadReg(GYRO_CTRL_REG1);
	gyroState &= 0xF8;
	gyroWriteReg(GYRO_CTRL_REG1, gyroState);
}

void gyroPowerDown(void){
	unsigned char gyroState = gyroReadReg(GYRO_CTRL_REG1);
	gyroState &= 0xF0;
	gyroWriteReg(GYRO_CTRL_REG1, gyroState);
}

void gyroWake(void){
	unsigned char gyroState = gyroReadReg(GYRO_CTRL_REG1);
	gyroState &= 0xF0;
	gyroState |= 0x0F;
	gyroWriteReg(GYRO_CTRL_REG1, gyroState);
}

unsigned char gyroDataReady(void){
	if(gyroReadReg(GYRO_STATUS_REG)&(1<<GYRO_ZYXDA)){
		return 1;
	}else{return 0;}
}

unsigned char gyroGetDataStatCheck(unsigned char output[], int index){
		deviceSelect(GYRO);
		unsigned char gotData = 0;
		unsigned char toSend = GYRO_STATUS_REG | increment | gyroRead;
		sPITradeByte(toSend);							//Address of status register (1 below data registers), w/autoincrement
		if(sPITradeByte(0x00)&(1<<GYRO_ZYXDA)){					//If data ready, increment through data registers
			for(int i=0; i<6; i++){
				output[i+index] = sPITradeByte(0x00);
			}
			gotData=1;
		}
		deselect();
		return gotData;
}