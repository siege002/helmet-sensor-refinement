/*
 * FTDITest.c
 *
 * Created: 8/18/2014 9:43:49 AM
 *  Author: Sam
 */ 


#include <avr/io.h>
#include <avr/interrupt.h>
#include "UARTLib.c"


/*FUSES =
{
	.low = 0xCF,		//Select External Ceramic Resonator, 8 MHz, no Clock Divide
	.high = 0xDF,		//Default settings
	.extended = 0xFF,	//Default settings
};
*/
void delay(unsigned long delayTime);
void initSysTime(void);
void millisChar(char output[]);

unsigned long millis;
unsigned long mark;
char timeToSend[21];
int dummy = 0;
long markTime;
int i;

int main(void)
{	
	DDRA |= (1<<PINA2);
	PORTA |= (1<<PINA2);
	initSysTime();
	uARTStart();
	char toSend[] = "UART TESTING\n\r";
    while(1)
    {
		mark = millis;
		millisChar(timeToSend);
		for(i = sizeof(timeToSend)-1; i>=0; i--){
			if(timeToSend[i] == '\f'){
				dummy = 1;
			}
			if(dummy == 1 && timeToSend[i] != '\f'){
				writeUart(timeToSend[i]);
			}
			
		}
		dummy = 0;
/*        for(int i=0; i<sizeof(toSend); i++){
			writeUart(toSend[i]);
		}
*/		while((millis-mark)<500){
		;
		}
    }
}

void delay(unsigned long delayTime){
	mark = millis;
	while((millis - mark) <= delayTime){
		;
	}
}

void initSysTime(void){
	TCCR0A = (1<<WGM01)|(1<<COM0A0);	//CTC mode  
	TCCR0B = (1<<CS02);		// Clock prescaler is 64 - 8 MHz/64 = 125 kHZ = 125 cycles/millisecond
	TCNT0 = 0x00;			//Initialize clock counter
	OCR0A = 0x7C;			//124 steps before counter reset
	ASSR = 0x00;
	TIMSK0 = (1<<OCIE0A);	//Enable Clock Compare Interrupts
	millis = 0;
	sei();
}

void millisChar(char output[]){
	markTime = millis;
	output[0] = '\n';
	output[1] = '\r';
	i = 2;
	while(markTime>0){
		output[i] = (markTime % 10) + '0';
		i++;
		markTime = markTime/10;
	}
	output[i] = '\f';
}


ISR(TIMER0_COMPA_vect){
	millis += 1;	
}

ISR(BADISR_vect){
	
}