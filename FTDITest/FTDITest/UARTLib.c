#include <avr/io.h>
void uARTStart(void);
void writeUart(char toSend);


void uARTStart(void){
DDRA |= (1<<1);
LINCR = 0x00;
LINBTR = (1<<LDISR)|(1<<LBT5)|(1<<LBT1)|(1<<LBT5);
LINBRRH = 0x00;
LINBRRL = (1<<LDIV0);		//35 samples/bit, (1+1) clk cycles per sample, 8 MHz clock, 115200 b/s
LINENIR = (1<<LENTXOK);
LINCR = (1<<LENA)|(1<<LCMD2)|(1<<LCMD0);
}

void writeUart(char toSend){
	while(LINSIR&(1<<LBUSY)){}
	LINDAT = toSend;
	while(LINSIR&(1<<LTXOK)){}
}
