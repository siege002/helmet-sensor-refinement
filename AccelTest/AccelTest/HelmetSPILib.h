//#ifndef HELMETH
//#define HELMETH

#define RADIOSELECTPIN 6
#define GYROSELECTPIN 6
#define ACCELSELECTPIN 7
#define MEMSELECTPIN 3
#define RADIO 0
#define GYRO 1
#define ACCEL 2
#define MEM 3


int sPIDevice;

void startSPI(void);
void deviceSelect(int device);
char sPITradeByte(char byte);
void deselect(void);

//#endif