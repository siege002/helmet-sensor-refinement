/*
 * AccelTest.c
 *
 * Created: 8/19/2014 10:42:46 AM
 *  Author: Sam
 */ 


#include <avr/io.h>
#include <avr/interrupt.h>
//#include "HelmetSPILib.c"
#include "ADXL375.c"
//#include "UARTLib.c"

void delay(unsigned long delayTime);
void initSysTime(void);

unsigned long mark;
unsigned long millis;
char accelRawData[200] = {0};
int i;
long timeStamp;
char neg = 0;
int count = 0; 

int main(void)
{
	initSysTime();
	delay(1000);
	uARTStart();
	delay(50);
	uartString("UART READY...\n\r",sizeof("UART READY...\n\r"));
	delay(50);
	startSPI();			
	delay(50);
	accelInitialize();
	delay(1000);
	char deviceTest =  accelReadReg(ACCEL_DEVID_REG);
	char devID[8] = "ATAD ON";
	for(int i=0; deviceTest!=0; i++){
		if((deviceTest%2)!=0){
			devID[i] = '1';
		}else{devID[i] = '0';}
		deviceTest = deviceTest/2;
	}
	uartString("ACCELEROMETER DEVICE ID:\n\r",sizeof("ACCELEROMETER DEVICE ID:\n\r"));
	uartReverse(devID, sizeof(devID));
	uartString("\n\n\r", sizeof("\n\n\r"));
	
    while(1)
    {
		char testing = accelReadReg(0x2E);
		

/*		char interrupts = accelCheckInterrupts();
		if(interrupts&ACCEL_SINGLE_SHOCK){
			mark = millis;
			i = 0;
			while((millis-mark<=10)&(i<=(sizeof(accelRawData)+6))){
				if(accelCheckInterrupts()&ACCEL_DATA_READY){
					accelGetData(accelRawData,i);
					i+=6;
				}
			}
			timeStamp = mark;
			uartString("SHOCK DETECTED!\n\rTIMESTAMP   X VALUE   Y VALUE   Z VALUE\n\r",sizeof("SHOCK DETECTED!\n\rTIMESTAMP   X VALUE   Y VALUE   Z VALUE\n\r"));
			char outputTime[9] = {0};
			int j=0;
			while(timeStamp!=0){
				outputTime[j] = timeStamp%10 + '0';
				timeStamp = timeStamp/10;
				j++;
			}
			uartReverse(outputTime, sizeof(outputTime));
			uartString("   ", sizeof("   "));
			for(int i=0; i<sizeof(accelRawData); i+=6){
				//X data formatting
				signed long xLSB = accelRawData[i];
				signed long xMSB = accelRawData[i+1];
				signed long xRaw = xLSB | (xMSB<<8);
				char xOut[7] = {0};
				if(xRaw & 0b10000000){
					xRaw = ~xRaw;
					xRaw++;
					xOut[0] = '-';
				}else{xOut[0] = '+';}
				float xScaledData = xRaw / 0.49;
				for(int j=1;j<4;j++){
					char format = (xScaledData/100)+'0';
					xScaledData = xScaledData*10.0;
					xOut[j] = format;
				}
				xOut[4] = '.';
				for(int j=5; j<sizeof(xOut); j++){
					char format = (xScaledData/100)+'0';
					xScaledData = xScaledData*10.0;
					xOut[j] = format;
				}
				//y data formatting
				signed long yLSB = accelRawData[i+2];
				signed long yMSB = accelRawData[i+3];
				signed long yRaw = yLSB | (yMSB<<8);
				char yOut[7] = {0};
				if(yRaw & 0b10000000){
					yRaw = ~yRaw;
					yRaw++;
					yOut[0] = '-';
					}else{yOut[0] = '+';}
				float yScaledData = yRaw / 0.49;
				for(int j=1;j<4;j++){
					char format = (yScaledData/100)+'0';
					yScaledData = yScaledData*10.0;
					yOut[j] = format;
				}
				yOut[4] = '.';
				for(int j=5; j<sizeof(yOut); j++){
					char format = (yScaledData/100)+'0';
					yScaledData = yScaledData*10.0;
					yOut[j] = format;
				}
				//z data formatting
				signed long zLSB = accelRawData[i+4];
				signed long zMSB = accelRawData[i+5];
				signed long zRaw = zLSB | (zMSB<<8);
				char zOut[7] = {0};
				if(zRaw & 0b10000000){
					zRaw = ~zRaw;
					zRaw++;
					zOut[0] = '-';
					}else{zOut[0] = '+';}
				float zScaledData = zRaw / 0.49;
				for(int j=1;j<4;j++){
					char format = (zScaledData/100)+'0';
					zScaledData = zScaledData*10.0;
					zOut[j] = format;
				}
				zOut[4] = '.';
				for(int j=5; j<sizeof(zOut); j++){
					char format = (zScaledData/100)+'0';
					zScaledData = zScaledData*10.0;
					zOut[j] = format;
				}
				//Printing remaining data
				uartString(xOut, sizeof(xOut));
				uartString("   ", sizeof("   "));
				uartString(yOut, sizeof(yOut));
				uartString("   ", sizeof("   "));
				uartString(zOut, sizeof(zOut));
				uartString("\n\n\r", sizeof("\n\n\r"));
				count = 0;
			}
		}else if(interrupts&ACCEL_ACTIVITY){
			count++;
			if(count>=500){
				uartString("ACCEL IN ACTIVE MODE - WAITING FOR HIT EVENT\n\r\n\r",sizeof("ACCEL IN ACTIVE MODE - WAITING FOR HIT EVENT\n\r\n\r"));
				count = 0;
			}
		}else{
			count ++;
			if(count>=32000){
				uartString("ACCEL ASLEEP - WAITING FOR ACTIVITY\n\r\n\r", sizeof("ACCEL ASLEEP - WAITING FOR ACTIVITY\n\r\n\r"));
				count = 0;
			}
		}
*/    }
}

void delay(unsigned long delayTime){
	mark = millis;
	while((millis - mark) <= delayTime){
		;
	}
}



void initSysTime(void){
	TCCR0A = (1<<WGM01);	//CTC mode
	TCCR0B = (1<<CS02);		//Clock prescaler is 64 - 8 MHz/64 = 125 kHZ = 125 cycles/millisecond
	TCNT0 = 0x00;			//Initialize clock counter
	OCR0A = 0x7C;			//124 steps before counter reset
	ASSR = 0x00;
	TIMSK0 = (1<<OCIE0A);	//Enable Clock Compare Interrupts
	millis = 0;
	sei();
}

ISR(TIMER0_COMPA_vect){
	millis += 1;
}

ISR(BADISR_vect){
	
}