#ifndef UARTLIB
#define UARTLIB
#include <avr/io.h>


void uARTStart(void);
void writeUart(char toSend);
void uartString(char toSend[], char bytes);
void uartReverse(char toSend[], char bytes);

void uARTStart(void){
DDRD |= (1<<PIND1); //Set TXD pin as output
UBRR0L = 0x03;		//Buad = Fosc/(2*(UBRR0+1)) - 16MHz/8 = 250000 BAUD
UBRR0H = 0x00;
UCSR0C = (1<<UCSZ01)|(1<<UCSZ00);	//Asynchronous operation, 8 data-bits, no parity, 1 stop bit
UCSR0B = (1<<TXEN0);				//Only TX mode enabled
	
}

void writeUart(char toSend){
	while(!(UCSR0A&(1<<TXC0))){}
	UCSR0A |= (1<<TXC0);
	UDR0 = toSend;
}

void uartString(char toSend[], char bytes){
	for(int p=0; p<bytes; p++){
		writeUart(toSend[p]);
	}
}

void uartReverse(char toSend[], char bytes){
	for(int p=(bytes-1);p>=0;p--){
		writeUart(toSend[p]);
	}
}

#endif