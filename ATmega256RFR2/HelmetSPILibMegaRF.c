/****************************************************
*****************************************************
*	HelmetSPILib.c
*
*/

#ifndef HELMETC
#define HELMETC
#include <avr/io.h>
#include "HelmetSPILibMegaRF.h"


void startSPI(void){
	//sPIDevice = 0;
	DDRE |= (1<<ACCELSELECTPIN)|(1<<GYROSELECTPIN);
	DDRB &= ~(1<<PINB3);	//Ensure MISO enabled as input
	DDRB |= (1<<PINB1)|(1<<PINB2)|(1<<PINB0);	//SCK, MOSI, SS all set as outputs
	PORTB |= (1<<PINB0);	//SS pullup enabled (so it isn't accidentally pulled low)
	//DDRB &= ~(1<<PINB4);
	PORTE |= (1<<MEMSELECTPIN)|(1<<ACCELSELECTPIN);
	SPCR = (1<<SPE)|(1<<MSTR);	//Clock Divide is 4 - SPI speed of 4 MHz
}

void deviceSelect(int device){
	switch(device){
		case RADIO:
		SPCR &= ~((1<<CPOL)|(1<<CPHA));
		PORTE &= ~(1<<RADIOSELECTPIN);
		break;
		case GYRO:
		SPCR |= ((1<<CPOL)|(1<<CPHA));
		PORTE &= ~(1<<GYROSELECTPIN);
		break;
		case ACCEL:
		SPCR |= ((1<<CPOL)|(1<<CPHA));
		PORTE &= ~(1<<ACCELSELECTPIN);
		break;
		case MEM:
		SPCR |= ((1<<CPOL)|(1<<CPHA));
		PORTE &= ~(1<<MEMSELECTPIN);
		break;
	}
}

char sPITradeByte(char byte){
	SPDR = byte;
	while(!(SPSR & (1<<SPIF))){}
	char received = SPDR;
	return received;
}

void deselect(void){
	PORTE |= (1<<GYROSELECTPIN)|(1<<ACCELSELECTPIN);
}
#endif