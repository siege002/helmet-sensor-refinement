#define L3GD20_REG_WHO_AM_I			0x0F
#define L3GD20_REG_CTRL_REG_1		0x20
#define	L3GD20_REG_CTRL_REG_2		0x21
#define L3GD20_REG_CTRL_REG_3		0x22
#define L3GD20_REG_CTRL_REG_4		0x23
#define L3GD20_REG_CTRL_REG_5		0x24
#define	L3GD20_REG_REFERENCE		0x25
#define L3GD20_REG_OUT_TEMP			0x26
#define L3GD20_REG_STATUS_REG		0x27
#define	L3GD20_REG_OUT_X_L			0x28
#define L3GD20_REG_OUT_X_H			0x29
#define L3GD20_REG_OUT_Y_L			0x2A
#define L3GD20_REG_OUT_Y_H			0x2B
#define L3GD20_REG_OUT_Z_L			0x2C
#define L3GD20_REG_OUT_Z_H			0x2D
#define L3GD20_REG_FIFO_CTRL_REG	0x2E
#define L3GD20_REG_FIFO_SRC_REG		0x2F
#define L3GD20_REG_INT1_CFG			0x30
#define L3GD20_REG_INT1_SRC			0x31
#define L3GD20_REG_INT1_TSH_XH		0x32
#define L3GD20_REG_INT1_TSH_XL		0x33
#define L3GD20_REG_INT1_TSH_YH		0x34
#define L3GD20_REG_INT1_TSH_YL		0x35
#define L3GD20_REG_INT1_TSH_ZH		0x36
#define L3GD20_REG_INT1_TSH_ZL		0x37
#define L3GD20_REG_INT1_DURATION	0x38

#define L3GD20_ODR_95HZ				0x00
#define L3GD20_ODR_190HZ			0x40
#define L3GD20_ODR_380HZ			0x80
#define L3GD20_ODR_760HZ			0xC0
#define L3GD20_BW_LOW				0x00
#define L3GD20_BW_MID_LOW			0x10
#define L3GD20_BW_MID_HIGH			0x20
#define L3GD20_BW_HIGH				0x30
#define L3GD20_PD_OFF				0x08
#define L3GD20_ZEN					0x04
#define L3GD20_XEN					0x02
#define L3GD20_YEN					0x01

#define L3GD20_HPM1					0x20
#define L3GD20_HPM0					0x10
#define L3GD20_HPCF3				0x08
#define L3GD20_HPCF2				0x04
#define L3GD20_HPCF1				0x02
#define L3GD20_HPCF0				0x01

#define L3GD20_I1_INT1				0x80
#define L3GD20_I1_BOOT				0x40
#define L3GD20_H_LACTIVE			0x20
#define L3GD20_PP_OD				0x10
#define L3GD20_I2_DRDY				0x08
#define L3GD20_I2_WTM				0x04
#define L3GD20_I2_ORUN				0x02
#define L3GD20_I2_EMPTY				0x01

#define L3GD20_BDU					0x80
#define L3GD20_BLE					0x40
#define L3GD20_250DPS				0x00
#define L3GD20_500DPS				0x10
#define L3GD20_2000DPS				0x20
#define L3GD20_SIM					0x01

#define L3GD20_BOOT					0x80
#define L3GD20_FIFO_EN				0x40
#define L3GD20_HPEN					0x10
#define L3GD20_INT1_SEL1			0x08
#define L3GD20_INT1_SEL0			0x04
#define L3GD20_OUT_SEL1				0x02
#define L3GD20_OUT_SEL0				0x01

#define L3GD20_ZYXOR				0x80
#define L3GD20_ZOR					0x40
#define L3GD20_YOR					0x20
#define L3GD20_XOR					0x10
#define L3GD20_ZYXDA				0x08
#define L3GD20_ZDA					0x04
#define L3GD20_YDA					0x02
#define L3GD20_XDA					0x01

#define L3GD20_FIFO_BYPASS			0x00
#define L3GD20_FIFO_NORMAL			0x20
#define L3GD20_FIFO_STREAM			0x40
#define L3GD20_FIFO_STR_TO_FIFO		0x60
#define L3GD20_FIFO_BYP_TO_STR		0x80

#define L3GD20_WTM					0x80
#define L3GD20_OVRN					0x04
#define L3GD20_EMPTY				0x20
#define L3GD20_FSS4					0x10
#define L3GD20_FSS3					0x08
#define L3GD20_FSS2					0x04
#define L3GD20_FSS1					0x02
#define L3GD20_FSS0					0x01

#define L3GD20_AND_OR				0x80
#define L3GD20_LIR					0x40
#define L3GD20_ZHIE					0x20
#define L3GD20_ZLIE					0x10
#define L3GD20_YHIE					0x08
#define L3GD20_YLIE					0x04
#define L3GD20_XHIE					0x02
#define L3GD20_XLIE					0x01

#define L3GD20_IA					0x40
#define L3GD20_ZH					0x20
#define L3GD20_ZL					0x10
#define L3GD20_YH					0x08
#define L3GD20_YL					0x04
#define L3GD20_XH					0x02
#define L3GD20_XL					0x01

#define L3GD20_WAIT					0x80
