#include <avr/io.h>
#include "L3GD20MegaRF.h"
#include "HelmetSPILibMegaRF.h"

#define GYRO_READ		0x80
#define GYRO_MULTIBYTE	0x40



char gyroDataReady(void){
	deviceSelect(GYRO);
	sPITradeByte(GYRO_READ|L3GD20_REG_STATUS_REG);
	char status = sPITradeByte(0);
	deselect();
	return status;
}

void gyroGetData(char * output[], int index){
	deviceSelect(GYRO);
	sPITradeByte(GYRO_READ|GYRO_MULTIBYTE|L3GD20_REG_OUT_X_L);
	for(char i=0;i<6;i++){
		*output[i] = sPITradeByte(0);
	}
	deselect;
}

void gyroInitialize(void){
	char gyroParams [] [2] = {
		{ L3GD20_REG_CTRL_REG_1, 0xF8 },
		{ GYRO_CTRL_REG2, 0x00 },
		//{ GYRO_CTRL_REG3, (1<<GYRO_I2_WTM) },
		{ GYRO_CTRL_REG4, (1<<GYRO_BDU)},
		{ GYRO_CTRL_REG5, (1<<GYRO_FIFO_EN) },
		{ GYRO_FIFO_CTRL_REG, 0 }, //Stream Mode, FIFO not empty interrupt threshold
		{ 255,0}
	};

	for(int i=0; gyroParams[i][0] != 255; i++){
		gyroWriteReg(gyroParams[i][0], gyroParams[i][1]);
	}
}