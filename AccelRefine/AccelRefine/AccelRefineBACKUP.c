/****************************************************
*****************************************************
*	HelmetSPILib.c
*
*/

#ifndef HELMETC
#define HELMETC
#include <avr/io.h>
#include "HelmetSPILib.h"


void startSPI(void){
	//sPIDevice = 0;
	DDRC |= (1<<ACCELSELECTPIN)|(1<<GYROSELECTPIN)|(1<<MEMSELECTPIN);
	DDRB |= (1<<PINB5)|(1<<PINB3)|(1<<PINB2);
	PORTB |= (1<<PINB2);
	//DDRB &= ~(1<<PINB4);
	PORTC |= (1<<MEMSELECTPIN)|(1<<ACCELSELECTPIN)|(1<<GYROSELECTPIN);
	SPSR = (1<<SPI2X);						//Double speed SPI
	SPCR = (1<<SPE)|(1<<MSTR);	//Clock Divide is 2 - SPI speed of 4 MHz
}

void deviceSelect(int device){
	switch(device){
		case RADIO:
		SPCR &= ~((1<<CPOL)|(1<<CPHA));
		PORTC &= ~(1<<RADIOSELECTPIN);
		break;
		case GYRO:
		SPCR |= ((1<<CPOL)|(1<<CPHA));
		PORTC &= ~(1<<GYROSELECTPIN);
		break;
		case ACCEL:
		SPCR |= ((1<<CPOL)|(1<<CPHA));
		PORTC &= ~(1<<ACCELSELECTPIN);
		break;
		case MEM:
		SPCR |= ((1<<CPOL)|(1<<CPHA));
		PORTC &= ~(1<<MEMSELECTPIN);
		break;
	}
}

char sPITradeByte(char byte){
	SPDR = byte;
	while(!(SPSR & (1<<SPIF))){}
	char received = SPDR;
	return received;
}

void deselect(void){
	PORTC |= (1<<GYROSELECTPIN)|(1<<ACCELSELECTPIN)|(1<<MEMSELECTPIN);
}
#endif