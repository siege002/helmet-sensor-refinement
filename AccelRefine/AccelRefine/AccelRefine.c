/*
 * GyroTest.c
 *
 * Created: 8/20/2014 12:18:04 PM
 *  Author: Sam
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
//#include "HelmetSPILib.c"
#include "L3G4200D_TINY87.c"
#include "ADXL375.c"
//#include "UARTLib.c"

void delay(unsigned long delayTime);
void initSysTime(void);

unsigned long mark;
unsigned long millis;
//long timeStampsAccel [52] = {0};
//long timeStampsGyro [16] = {0};	








int main(void)
{
	startSPI();
	initSysTime();
	delay(100);
	uARTStart();
	accelInitialize();
	delay(10);
	uartString("UART READY...\n\r",sizeof("UART READY...\n\r"));
	delay(10);
	uartString("ACCEL INITIALIZED...\n\r",sizeof("ACCEL INITIALIZED...\n\r"));
	char deviceTest =  accelReadReg(ACCEL_DEVID_REG);
	char devID[8] = "ATAD ON";
	for(int i=0; deviceTest!=0; i++){
		if((deviceTest%2)!=0){
			devID[i] = '1';
			}else{devID[i] = '0';}
			deviceTest = deviceTest/2;
		}
	uartString("ACCEL DEVICE ID:  ",sizeof("ACCEL DEVICE ID:  "));
	uartReverse(devID,sizeof(devID));
	uartString("\n\r", sizeof("\n\r"));
	gyroInitialize();
	uartString("GYRO INITIALIZED...\n\r",sizeof("GYRO INITIALIZED...\n\r"));
	char gyroTest = gyroReadReg(GYRO_WHO_AM_I);
	char gyroID[8] = "ADAT ON";
	for(int i=0; i<8; i++){
		gyroID[i] = gyroTest%2 + '0';
		gyroTest /= 2;
	}
	uartString("GYRO DEVICE ID:  ",sizeof("GYRO DEVICE ID:  "));
	uartReverse(gyroID,sizeof(gyroID));
	uartString("\n\n\r", sizeof("\n\n\r"));
	uartString("TIME(ms),    Xacc(g),   Yacc(g),   Zacc(g),   XGyro,       YGyro,       ZGyro\n\n\r",sizeof("TIME(ms),    Xacc(g),   Yacc(g),   Zacc(g),   XGyro,       YGyro,       ZGyro\n\n\r"));	
    while(1)
    {
			char accelRawData[288] = {0};
			char gyroRawData[72] = {0};
			char gyroTrash[6] = {0};
			int a=0;
			int b=0;
			int c = 0;
			if(accelCheckInterrupts()&ACCEL_SINGLE_SHOCK){
			mark = millis;
			while((a<288)|(b<228)){
				if(a<288){
					if(accelGetDataWithCheck(accelRawData,a)){
						a += 6;
					}
				}
				if(b<156){
					if(gyroGetDataStatCheck(gyroTrash,0)){
						b += 6;
					}
				}else{
					if(gyroGetDataStatCheck(gyroRawData,(b-156))){
						b += 6;
					}
				}
			}
			char timeDec[10] = {0};
			for(int j=0; j<10; j++){
				timeDec[j] = (mark % 10) + '0';
				mark = mark/10;
			}
			uartReverse(timeDec,sizeof(timeDec));
			uartString(" , ",sizeof(" , "));
			for(int i =0; i<(a/6); i++){

				char xDec[8] = {0};
				char yDec[8] = {0};
				char zDec[8] = {0};
				short xUnfilt = accelRawData[1+(6*i)];
				xUnfilt = xUnfilt<<8;
				xUnfilt |= accelRawData[0+(6*i)];
				short yUnfilt = accelRawData[3+(6*i)];
				yUnfilt = yUnfilt<<8;
				yUnfilt |= accelRawData[2+(6*i)];
				short zUnfilt = accelRawData[5+(6*i)];
				zUnfilt = zUnfilt<<8;
				zUnfilt |= accelRawData[4+(6*i)];
				if(xUnfilt&0x8000){
					xUnfilt = ~xUnfilt;
					xUnfilt += 1;
					xDec[7] = '-';
				}else{xDec[7] = '+';}
				if(yUnfilt&0x8000){
					yUnfilt = ~yUnfilt;
					yUnfilt += 1;
					yDec[7] = '-';
				}else{yDec[7] = '+';}
				if(zUnfilt&0x8000){
					zUnfilt = ~zUnfilt;
					zUnfilt += 1;
					zDec[7] = '-';
				}else{zDec[7] = '+';}
				long xUnfiltFull = 0; 
				xUnfiltFull|= (xUnfilt>>1);
				xUnfiltFull *= 49;
				long yUnfiltFull = 0;
				yUnfiltFull|= (yUnfilt>>1);
				yUnfiltFull *= 49;
				long zUnfiltFull = 0;
				zUnfiltFull |= (zUnfilt>>1);
				zUnfiltFull *= 49;
				for(int j =0; j<3; j++){
					xDec[j] = (xUnfiltFull%10)+'0';
					yDec[j] = (yUnfiltFull%10)+'0';
					zDec[j] = (zUnfiltFull%10)+'0';
					xUnfiltFull = xUnfiltFull/10;
					yUnfiltFull = yUnfiltFull/10;
					zUnfiltFull = zUnfiltFull/10;
				}
				xDec[3] = '.';
				yDec[3] = '.';
				zDec[3] = '.';
				for(int j = 4; j<7; j++){
					xDec[j] = (xUnfiltFull%10)+'0';
					yDec[j] = (yUnfiltFull%10)+'0';
					zDec[j] = (zUnfiltFull%10)+'0';
					xUnfiltFull = xUnfiltFull/10;
					yUnfiltFull = yUnfiltFull/10;
					zUnfiltFull = zUnfiltFull/10;
				}

				uartReverse(xDec,sizeof(xDec));
				uartString(" , ",sizeof(" , "));
				uartReverse(yDec,sizeof(yDec));
				uartString(" , ",sizeof(" , "));
				uartReverse(zDec,sizeof(zDec));
				uartString(" , ",sizeof(" , "));
				if((c+5)%4 != 0){
					uartString("\n\r            ,",sizeof("\n\r            ,"));
				}
				else{
					char GxDec[10] = {0};
					char GyDec[10] = {0};
					char GzDec[10] = {0};
					short xGUnfilt = gyroRawData[1+(6*c)];
					xGUnfilt = xGUnfilt <<8;
					xGUnfilt |= gyroRawData[0+(6*c)];
					short yGUnfilt = gyroRawData[3+(6*c)];
					yGUnfilt = yGUnfilt<<8;
					yGUnfilt |= gyroRawData[2+(6*c)];
					short zGUnfilt = gyroRawData[5+(6*c)];
					zGUnfilt = zGUnfilt<<8;
					zGUnfilt |= gyroRawData[4+(6*c)];
					if(xGUnfilt&0x8000){
						xGUnfilt = ((~xGUnfilt) + 1)&0xFF;
						GxDec[9] = '-';
					}else{GxDec[9] = '+';}
					if(yGUnfilt&0x8000){
						yGUnfilt = ((~yGUnfilt) + 1)&0xFF;
						GyDec[9] = '-';
					}else{GyDec[9] = '+';}
					if(zGUnfilt&0x8000){
						zGUnfilt = ((~zGUnfilt) + 1)&0xFF;
						GzDec[9] = '-';
					}else{GzDec[9] = '+';}
					long xGUnfiltFull = 0;
					xGUnfiltFull |= xGUnfilt;
					xGUnfiltFull *= 875;
					long yGUnfiltFull = 0;
					yGUnfiltFull|= yGUnfilt;
					yGUnfiltFull *= 875;
					long zGUnfiltFull = 0;
					zGUnfiltFull|= zGUnfilt;
					zGUnfiltFull *= 875;
					for(int j =0; j<5; j++){
						GxDec[j] = (xGUnfiltFull%10)+'0';
						GyDec[j] = (yGUnfiltFull%10)+'0';
						GzDec[j] = (zGUnfiltFull%10)+'0';
						xGUnfiltFull = xGUnfiltFull/10;
						yGUnfiltFull = yGUnfiltFull/10;
						zGUnfiltFull = zGUnfiltFull/10;
					}
					GxDec[5] = '.';
					GyDec[5] = '.';
					GzDec[5] = '.';
					for(int j = 6; j<9; j++){
						GxDec[j] = (xGUnfiltFull%10)+'0';
						GyDec[j] = (yGUnfiltFull%10)+'0';
						GzDec[j] = (zGUnfiltFull%10)+'0';
						xGUnfiltFull = xGUnfiltFull/10;
						yGUnfiltFull = yGUnfiltFull/10;
						zGUnfiltFull = zGUnfiltFull/10;
					}
					uartReverse(GxDec,sizeof(GxDec));
					uartString(" , ",sizeof(" , "));
					uartReverse(GyDec,sizeof(GyDec));
					uartString(" , ",sizeof(" , "));
					uartReverse(GzDec,sizeof(GzDec));
					uartString("\n\r            ,",sizeof("\n\r            ,"));
				}
				c++;
			}
		}			
	}
}

void delay(unsigned long delayTime){
	mark = millis;
	while((millis - mark) <= delayTime){
		;
	}
}



void initSysTime(void){
	TCCR0A = (1<<WGM01);//|(1<<COM0A0);	//CTC mode
	TCCR0B = (1<<CS00)|(1<<CS01);		//Clock prescaler is 64 - 8 MHz clock, 125 cycles per mS
	OCR0A = 0x7C;			//7 steps before counter reset
	ASSR = 0x00;
	TCNT0 = 0x00;			//Initialize clock counter
	TIMSK0 = (1<<OCIE0A);	//Enable Clock Compare Interrupts
	millis = 0;
	sei();
}

ISR(TIMER0_COMPA_vect){
	millis += 1;
}

ISR(BADISR_vect){
	;
}