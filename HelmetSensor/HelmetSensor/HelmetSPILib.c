/****************************************************
*****************************************************
*	HelmetSPILib.c
*
*/


#include <avr/io.h>


#define RADIOSELECTPIN 6
#define GYROSELECTPIN 1
#define ACCELSELECTPIN 0
#define MEMSELECTPIN 7
#define RADIO 0
#define GYRO 1
#define ACCEL 2
#define MEM 3
#define ARD 4

void startSPI(void);
void deviceSelect(int device);
char sPITradeByte(char byte);
void deselect(void);

int sPIDevice;

void startSPI(void){
	DDRA = (1<<RADIOSELECTPIN)|(1<<MEMSELECTPIN)|(1<<4)|(1<<5);
	DDRA &= ~(1<<2);
	DDRB |= (1<<ACCELSELECTPIN)|(1<<GYROSELECTPIN);
	PORTA |= (1<<RADIOSELECTPIN)|(1<<MEMSELECTPIN);
	PORTB |= (1<<ACCELSELECTPIN)|(1<<GYROSELECTPIN);
	SPCR = (1<<SPE)|(1<<MSTR);
	sPIDevice = 0;
}

void deviceSelect(int device){
	switch(device){
		case RADIO:
		SPCR &= ~((1<<CPOL)|(1<<CPHA));
		PORTA &= ~(1<<RADIOSELECTPIN);
		sPIDevice = RADIO;
		break;
		case GYRO:
		SPCR |= ((1<<CPOL)|(1<<CPHA));
		PORTB &= ~(1<<GYROSELECTPIN);
		sPIDevice = GYRO;
		break;
		case ACCEL:
		SPCR |= ((1<<CPOL)|(1<<CPHA));
		PORTB &= ~(1<<ACCELSELECTPIN);
		sPIDevice = ACCEL;
		break;
		case MEM:
		SPCR |= ((1<<CPOL)|(1<<CPHA));
		PORTA &= ~(1<<MEMSELECTPIN);
		sPIDevice = MEM;
		break;
	}
}

char sPITradeByte(char byte){
	SPDR = byte;
	while(!(SPSR & (1<<SPIF) )){}
	char received = SPDR;
	return received;
}

void deselect(void){
	switch(sPIDevice){
		case RADIO:
		PORTA |= (1<<RADIOSELECTPIN);
		break;
		case GYRO:
		PORTB |= (1<<GYROSELECTPIN);
		break;
		case ACCEL:
		PORTB |= (1<<ACCELSELECTPIN);
		break;
		case MEM:
		PORTA |= (1<<MEMSELECTPIN);
		break;
	}
}