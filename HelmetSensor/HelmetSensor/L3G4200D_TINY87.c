/* 	L3G4200D Library 
/	for use with ATtiny87 SPI communication
/	30 June, 2014
/	Sam Siegel
/	Basic Operation:  Gyro runs in stream mode, generates and interrupt (INT2) when the FIFO is not empty.
/	Main program should periodically check the interrupt pin and read data out until the interrupt pin goes low.
/	
*/


#include <avr/io.h>
#include "L3G4200D_TINY87.h"
#include "HelmetSPILib.c"

#define gyroDataReadyPin	3		//PORTB, PB3
#define gyroSlaveSelectPin 	1		//PORTB, PB1
#define gyroRead			0x80
#define gyroWrite			0x00
#define increment	 		0x40
#define noIncrement			0x00


//Fills a supplied array with gyro data, starting at the given index number
void gyroGetData(char output[], int index){
	deviceSelect(GYRO);
	char toSend = GYRO_OUT_X_L | increment | gyroWrite;
	sPITradeByte(toSend);							//Address of Xlow register, w/autoincrement
	output[0+index] = sPITradeByte(0);				//get first byte of data
	output[1+index] = sPITradeByte(0);				//etc...
	output[2+index] = sPITradeByte(0);
	output[3+index] = sPITradeByte(0);
	output[4+index] = sPITradeByte(0);
	output[5+index] = sPITradeByte(0);
	deselect();
	}

// Returns current temperature of gyro	
char gyroGetTemp(void){
	char gyroTemp;
	deviceSelect(GYRO);
	char tempAddress = GYRO_OUT_TEMP | gyroRead;
	sPITradeByte(tempAddress);
	gyroTemp = sPITradeByte(0);
	deselect();
	return gyroTemp;
	}

void gyroWriteReg(char address, char data){
	address |= gyroWrite;
	deviceSelect(GYRO);
	sPITradeByte(address);
	sPITradeByte(data);
	deselect();
}

void gyroInitialize(void){
	DDRB &= ~(1<<gyroDataReadyPin);	//Connected to INT2 of Gyro, configured as input
	char gyroParams [] [2] = {
		{ GYRO_CTRL_REG1, 0xF8 },
		{ GYRO_CTRL_REG2, 0x00 },
		{ GYRO_CTRL_REG3, (1<<GYRO_I2_WTM) },
		{ GYRO_CTRL_REG4, (1<<GYRO_BDU)|(1<<GYRO_BLE) },
		{ GYRO_CTRL_REG5, (1<<GYRO_FIFO_EN) },
		{ GYRO_FIFO_CTRL_REG, (1<<GYRO_FM1)|(1<<GYRO_WTM0) }, //Stream Mode, FIFO not empty interrupt threshold
		{ 255,0}
	};

	for(int i=0; gyroParams[i][0] != 255; i++){
		gyroWriteReg(gyroParams[i][0], gyroParams[i][1]);
	}

}

char gyroReadReg(char address){
	address |= gyroRead;
	deviceSelect(GYRO);
	sPITradeByte(address);
	char contents = sPITradeByte(0x00);
	return contents;
}

void gyroSleep(void){
	char gyroState = gyroReadReg(GYRO_CTRL_REG1);
	gyroState &= 0xF8;
	gyroWriteReg(GYRO_CTRL_REG1, gyroState);
}

void gyroPowerDown(void){
	char gyroState = gyroReadReg(GYRO_CTRL_REG1);
	gyroState &= 0xF0;
	gyroWriteReg(GYRO_CTRL_REG1, gyroState);
}

void gyroWake(void){
	char gyroState = gyroReadReg(GYRO_CTRL_REG1);
	gyroState &= 0xF0;
	gyroState |= 0x0F;
	gyroWriteReg(GYRO_CTRL_REG1, gyroState);
}

char gyroDataReady(void){
	if(PINA&(1<<PINA3)){
		return 1;
	}
	else{return 0;}
}