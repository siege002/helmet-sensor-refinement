/*
 * HelmetSensor.c
 *
 * Created: 8/12/2014 12:34:55 PM
 *  Author: Sam
 */ 
#include <avr/io.h>
#include <avr/interrupt.h>
#include "L3G4200D_TINY87.c"
#include "MX25L4006E_TINY87.c"
#include "ADXL375.c"
#include "UARTLib.c"
#define SYNC "Main"
#define RECEIVER 0x18

FUSES =
{
	.low = 0xCF,		//Select External Ceramic Resonator, 8 MHz, no Clock Divide
	.high = 0xDF,		//Default settings
	.extended = 0xFF,	//Default settings
};

//Variable Definitions
unsigned long millis;
char setupComplete = 0;
char turnOn = 0;
unsigned long mark = 0;
unsigned long markTime = 0;
char gyroRawData[512] = {0};
char accelRawData[512] = {0};
int accelIndex = 0;
int gyroIndex = 0;
char sending = 0;
char LED = 0;
char timestampLSB;
char timestampMLB;
char timestampMHB;
char timestampMSB;

//State and event definitions
enum states {OFF, SETUP, WAIT, AWAKE, GET_DATA, STORE_DATA, EVENT_TIME_OUT, SEND_DATA, SLEEP, MAX_STATES} currentState;
enum events {POWER_SWITCH_ON, POWER_SWITCH_OFF, SETUP_COMPLETE, ACTIVITY, HIT_EVENT, GET_DATA_COMPLETE, STORE_COMPLETE, DATA_READY, TIME_OUT, SEND_COMPLETE, MAX_EVENTS} newEvent;

//function prototypes
char activityInt(void);
char hitEventInt(void);
char offButton(void);

//actions for State table - function prototypes
enum events getNewEvent(void);
void actionOffPowerSwitchOn(void);
void actionOffNullEvent(void);
void actionAnyStatePowerSwitchOff(void);
void actionSetupSetupComplete(void);
void actionSetupNullEvent(void);
void actionWaitActivity(void);
void actionWaitNullEvent(void);
void actionWaitTimeOut(void);
void actionAwakeHitEvent(void);
void actionAwakeNullEvent(void);
void actionGetDataGetDataComplete(void);
void actionGetDataNullEvent(void);
void actionStoreDataStoreComplete(void);
void actionStoreDataNullEvent(void);
void actionEventTimeOutTimeOut(void);
void actionEventTimeOutActivity(void);
void actionEventTimeOutHitEvent(void);
void actionEventTimeOutDataReady(void);
void actionEventTimeOutNullEvent(void);
void actionSendDataHitEvent(void);
void actionSendDataSendComplete(void);
void actionSendDataNullEvent(void);
void actionSleepActivity(void);
void actionSleepNullEvent(void);
void initSysTime(void);


//State Table Definition
void (*const state_table [MAX_STATES][MAX_EVENTS]) (void) = {
	{actionOffPowerSwitchOn,actionOffNullEvent,actionOffNullEvent,actionOffNullEvent,actionOffNullEvent,actionOffNullEvent,actionOffNullEvent,actionOffNullEvent,actionOffNullEvent,actionOffNullEvent},
	{actionSetupNullEvent,actionAnyStatePowerSwitchOff,actionSetupSetupComplete,actionSetupNullEvent,actionSetupNullEvent,actionSetupNullEvent,actionSetupNullEvent,actionSetupNullEvent,actionSetupNullEvent,actionSetupNullEvent},
	{actionWaitNullEvent,actionAnyStatePowerSwitchOff,actionWaitNullEvent,actionWaitActivity,actionWaitNullEvent,actionWaitNullEvent,actionWaitNullEvent,actionWaitNullEvent,actionWaitNullEvent,actionWaitNullEvent},
	{actionAwakeNullEvent,actionAnyStatePowerSwitchOff,actionAwakeNullEvent,actionAwakeNullEvent,actionAwakeHitEvent,actionAwakeNullEvent,actionAwakeNullEvent,actionAwakeNullEvent,actionAwakeNullEvent,actionAwakeNullEvent},
	{actionGetDataNullEvent,actionAnyStatePowerSwitchOff,actionGetDataNullEvent,actionGetDataNullEvent,actionGetDataNullEvent,actionGetDataGetDataComplete,actionGetDataNullEvent,actionGetDataNullEvent,actionGetDataNullEvent,actionGetDataNullEvent},
	{actionStoreDataNullEvent,actionAnyStatePowerSwitchOff,actionStoreDataNullEvent,actionStoreDataNullEvent,actionStoreDataNullEvent,actionStoreDataNullEvent,actionStoreDataStoreComplete,actionStoreDataNullEvent,actionStoreDataNullEvent,actionStoreDataNullEvent},
	{actionEventTimeOutNullEvent,actionAnyStatePowerSwitchOff,actionEventTimeOutNullEvent,actionEventTimeOutActivity,actionEventTimeOutHitEvent,actionEventTimeOutNullEvent,actionEventTimeOutNullEvent,actionEventTimeOutDataReady,actionEventTimeOutTimeOut,actionEventTimeOutNullEvent},
	{actionSendDataNullEvent,actionAnyStatePowerSwitchOff,actionSendDataNullEvent,actionSendDataNullEvent,actionSendDataHitEvent,actionSendDataNullEvent,actionSendDataNullEvent,actionSendDataNullEvent,actionSendDataNullEvent,actionSendDataSendComplete},
	{actionSleepNullEvent,actionAnyStatePowerSwitchOff,actionSleepNullEvent,actionSleepActivity,actionSleepNullEvent,actionSleepNullEvent,actionSleepNullEvent,actionSleepNullEvent,actionSleepNullEvent,actionSleepNullEvent}
};

int main(void){
	
	while(1){
		newEvent = getNewEvent();
		if((newEvent>=0)&&(newEvent<MAX_EVENTS)&&(currentState>=0)&&(currentState<MAX_STATES)){
			state_table [currentState][newEvent]();
		}
		else{
			//Error - invalid state or event
		}
	}
}

void actionOffPowerSwitchOn(void){
	SMCR &= ~(1<<SE);		//Turn off power down enable
	PCMSK0 = 0x00;			//Turn off Pin change interrupt
	initSysTime();
	while(millis<=50){};
	gyroInitialize();
	accelInitialize();
	uARTStart();
	DDRB |= (1<<6);		//Power indicator LED
	PORTB |= (1<<6);	//Power indicator LED
	setupComplete = 1;
	currentState = SETUP;
}

void actionOffNullEvent(void){
	//Do Nothing
}

void actionAnyStatePowerSwitchOff(void){
	gyroPowerDown();
	accelSleep();
	PORTB &= ~(1<<6);		//Power indicator LED OFF
	currentState = OFF;
	PCMSK0 = (1<<PCINT7);
	PCICR = (1<<PCIE0);
	mark = millis;
	while(millis-mark<10){}
	TIMSK0 = 0x00;
	millis = 0;
	SMCR = (1<<SM1);
	SMCR |= (1<<SE);
}

void actionSetupSetupComplete(void){
	mark = millis;
	markTime = 50;
	currentState = WAIT;
}

void actionSetupNullEvent(void){
	//Do nothing - wait for setup to complete
}

void actionWaitActivity(void){
	gyroWake();
	mark = millis;
	markTime = 1000;
	currentState = AWAKE;
}

void actionWaitNullEvent(void){
	//Do nothing
}

void actionWaitTimeOut(void){
	gyroSleep();
	currentState = SLEEP;
}

void actionAwakeHitEvent(void){
	mark = millis;
	markTime = 15;
	currentState = GET_DATA;
}

void actionAwakeNullEvent(void){
	currentState = EVENT_TIME_OUT;
	//Do nothing
}

void actionGetDataGetDataComplete(void){
	//Perform actions to move into STORE_DATA state
	currentState = STORE_DATA;
}

void actionGetDataNullEvent(void){
	//do nothing
}

void actionStoreDataStoreComplete(void){
	sending = 1;
	currentState = EVENT_TIME_OUT;
}

void actionStoreDataNullEvent(void){
	//Do Nothing
}

void actionEventTimeOutTimeOut(void){
	gyroSleep();
	currentState = SLEEP;
}

void actionEventTimeOutActivity(void){
	gyroWake();
	mark = millis;
	markTime = 1000;
	currentState = ACTIVITY;
}

void actionEventTimeOutHitEvent(void){
	mark = millis;
	markTime = 15;
	currentState = GET_DATA;
}

void actionEventTimeOutDataReady(void){
	//Perform actions to move into SEND_DATA state
	currentState = SEND_DATA;
}

void actionEventTimeOutNullEvent(void){
	//Do nothing
}

void actionSendDataHitEvent(void){
	//Perform actions to move into GET_DATA state
	currentState = GET_DATA;
}

void actionSendDataSendComplete(void){
	currentState = EVENT_TIME_OUT;
}

void actionSendDataNullEvent(void){
	//Do Nothing
}

void actionSleepActivity(void){
	gyroWake();
	mark = millis;
	markTime = 1000;
	currentState = AWAKE;
}

void actionSleepNullEvent(void){
	//Do Nothing
}

enum events getNewEvent(void){

	switch(currentState){
		case OFF:
			if(turnOn){			//Need to define ON_BUTTON macro - find a pin to determine whether device has been turned on
				return POWER_SWITCH_ON;
			}
			else{
				return TIME_OUT;
			}
			break;
		
		case SETUP:
			return SETUP_COMPLETE;
			break;
		case WAIT:
			if(activityInt()){		//need to define function that polls pin defined for activity interrupt from Accel
				return ACTIVITY;
			}
			else if(offButton()){			//See SETUP case - need definition of macro
				return POWER_SWITCH_OFF;
			}
			else if((millis-mark)>=markTime){	
				return TIME_OUT;
			}
			else{
				return DATA_READY;			// Null event - remain in state
			}
			break;
		
		case AWAKE:
			if(hitEventInt()){				//need to define function to poll for hit event interrupt
				return HIT_EVENT;
			}
			else if(offButton()){			//See SETUP case - need definition of macro
				return POWER_SWITCH_OFF;
			}
			else{return TIME_OUT;}			//Null Event - Moves to eventTimeOut state
			break;
	
		case GET_DATA:
			if(millis-mark<=markTime){
				if(accelCheckInterrupts()&ACCEL_DATA_READY){
					accelGetData(accelRawData,accelIndex);
					accelIndex += 6;
				}
				if(gyroDataReady()){
					gyroGetData(gyroRawData,gyroIndex);
					gyroIndex += 6;
				}
			return TIME_OUT;		//null event
			}
			else{
				timestampLSB = mark&0xFF;
				timestampMLB = (mark>>8)&0xFF;
				timestampMHB = (mark>>16)&0xFF;
				timestampMSB = (mark>>24)&0xFF;
				return GET_DATA_COMPLETE;
				}
			break;	
			
		case STORE_DATA:{
			char accelData1[256] = {0};
			char accelData2[256] = {0};
			char gyroData1[256] = {0};	
			char gyroData2[256] = {0};
			for(int i=0; i<256; i++){
				accelData1[i] = accelRawData[i];
				gyroData1[i] = gyroRawData[i];
				accelData2[i] = gyroRawData[i+256];
				gyroData2[i] = gyroRawData[i+256];
			}
			accelData2[252] = timestampMSB;
			accelData2[253] = timestampMHB;
			accelData2[254] = timestampMLB;
			accelData2[255] = timestampLSB;
			memWritePage(accelData1,0x010000);
			memWritePage(accelData2,0x0101000);
			memWritePage(gyroData1, 0x0102000);
			memWritePage(gyroData2, 0x0103000);
			sending = 1;
			return STORE_COMPLETE;
			}
			break;
	

		case EVENT_TIME_OUT:
			if(sending!=0){
				return DATA_READY;
			}
			else if(activityInt()){				//Need function as defined in WAIT state
				return ACTIVITY;
			}
			else if(hitEventInt()){			//Need function as defined in AWAKE state
				return HIT_EVENT;
			}
			else if(offButton()){			//See SETUP case - need definition of macro
				return POWER_SWITCH_OFF;
			}
			else if((millis-mark)>=markTime){	//Need function as defined in WAIT case
				return TIME_OUT;
			}
			else {
				return GET_DATA_COMPLETE;	//Null event - remain in state
			}
			break;
	
		case SEND_DATA:
			if(offButton()){
				return POWER_SWITCH_OFF;
			}
			else{
			char memOut[1024];
			memReadData(0x010000,1024,memOut,0);
			for(int i = 0; i<1024; i++){
				char toSend = memOut[i];
				writeUart(toSend);
			}
			LED = !LED;
			PORTB &= ~(1<<6);
			PORTB |= (LED<<6);
			mark = millis;
			while((millis-mark)<=750){}
			return TIME_OUT;
			}
			break;
	
		case SLEEP:
			if(offButton()){
				return POWER_SWITCH_OFF;
			}
			if(activityInt()){				//Defined in WAIT state
				return ACTIVITY;
			}
			else{
				return TIME_OUT;			//Null event - remain in state
			}
			break;	
	}
}




char activityInt(void){
	char accelStat = accelCheckInterrupts();
	if(accelStat&ACCEL_ACTIVITY){
		return 1;
	}
	else{return 0;}
}

char hitEventInt(void){
	char accelStat = accelCheckInterrupts();
	if(accelStat&ACCEL_SINGLE_SHOCK){
		return 1;
	}
	else{return 0;}
}

char offButton(void){
	if(PINA&(1<<PINA7)){
		mark = millis;
		while((millis-mark)>=8){}
		while(PINA&(1<<PIN7)){}
		mark = millis;
		while((millis-mark>=8)){}
		return 1;
	}
	else{return 0;}
}

void initSysTime(void){
	TCCR0A = (1<<WGM01);	//CTC mode
	TCCR0B = (1<<CS02);		// Clock prescaler is 64 - 8 MHz/64 = 125 kHZ = 125 cycles/millisecond
	TCNT0 = 0x00;			//Initialize clock counter
	OCR0A = 0x7C;			//124 steps before counter reset
	TIMSK0 = (1<<OCIE0A);	//Enable Clock Compare Interrupts
	millis = 0;
	sei();
}

ISR (TIMER0_COMPA_vect){
	millis++;
}

ISR (PCINT0_vect){
	for(int i=0; i<8; i++){
		for(int j=0; j<8000; j++){
			i+1;		//NOP
		}
	}
	while(PINA&PINA7){}
	for(int i=0; i<8; i++){
		for(int j=0; j<8000; j++){
			i+1;		//NOP
		}
	}
	turnOn = 1;
}

ISR(BADISR_vect){
	
}