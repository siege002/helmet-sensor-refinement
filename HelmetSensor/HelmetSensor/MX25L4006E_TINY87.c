/* 	MX25L4006E Library 
/	Macronix Flash Memory - for use with ATtiny87 SPI communication
/	1 July, 2014
/	Sam Siegel
*/


#include <avr/io.h>
#include "MX25L4006E_TINY87.h"
#include "HelmetSPILib.c"

#define memSlaveSelectPin	7	//PORTA, pin 7




/*	memReadData
	Reads specified number of bytes starting at specified address
	Param: address - 3-byte long, bytes - number of bytes to read, output - array to store output data, index - beginning index for output write
*/
void memReadData(long address, int bytes, char output[], int index)
	{
	char LSB = (address>>16)&0xFF;
	char MID = (address>>8)&0xFF;
	char MSB = address & 0xFF;
	deviceSelect(MEM);
	sPITradeByte(MEM_READ);					
	sPITradeByte(LSB);			//Write first byte of address	
	sPITradeByte(MID);			//Second byte
	sPITradeByte(MSB);			//Third byte
	for(int i=0;i<bytes;i++)
		{
		output[i+index] = sPITradeByte(0);
		}
	deselect();
	}


/*	memWritePage
	Writes 256 bytes starting at the beginning of the Specified page.
	Param: 	data - 256 byte array to be written
			address - 3-byte long defining page to write
*/
void memWritePage(char data[static 256], long address)
	{
	memWaitForWrite();		//Do nothing until memory ready
	
	deviceSelect(MEM);			//Ensure write enabled			
	sPITradeByte(MEM_WREN);
	deselect();
	
	memWaitForWrite();

	char LSB = (address>>16)&0xFF;
	char MID = (address>>8)&0xFF;
	char MSB = 0x00;
	deviceSelect(MEM);
	sPITradeByte(MEM_PP);
	sPITradeByte(LSB);		//Write first byte of address
	sPITradeByte(MID);		//Second byte				
	sPITradeByte(MSB);		//Third byte = zero, beginning of page
	char toSend;
	for(int i=0; i<256; i++)
		{
		toSend = data[i];
		sPITradeByte(toSend);
		}
	deselect();
	}

/*	memWriteBytes
	Writes the specified number of bytes to the specified address.
	If number of bytes is larger than the page length (256-the two least
	significant bytes of address), remainder of data is written to the 
	beginning of the page.
	Param: 	data[] - array of bytes to be written
			size - number of bytes to write
			address - memory address to write to
*/
void memWriteBytes(char data[], int size, long address)
	{
	memWaitForWrite();
	deviceSelect(MEM);
	sPITradeByte(MEM_WREN);
	deselect();
	memWaitForWrite();
	deviceSelect(MEM);
	sPITradeByte(MEM_PP);
	char MSB = (address>>16)&0xFF;
	char MID = (address>>8)&0xFF;
	char LSB = address&0xFF;
	sPITradeByte(MSB);
	sPITradeByte(MID);
	sPITradeByte(LSB);
	for(int i=0; i<size; i++)
		{
		char toSend = data[i];
		sPITradeByte(toSend);
		}
	deselect();
	}


/*	memWaitForWrite
	Waits until Write In Progress Ends
*/
void memWaitForWrite(void)
	{
	int status;
	int ready = 1;
	deviceSelect(MEM);
	sPITradByte(MEM_RDSR);
	status = sPITradeByte(0);
	ready = status & (1<<MEM_WIP);
	//write in progress if stat reg. byte 0 returns 1
	while(ready == 1)
		{
		status = sPITradeByte(0);
		ready = status & (1<<MEM_WIP);
		}
	deselect();
	}